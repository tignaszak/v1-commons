plugins {
    `maven-publish`
}

dependencies {
    implementation(project(":conf"))
    implementation(project(":error"))
    implementation(project(":jwt"))
    implementation(project(":rest"))
    implementation(project(":spring"))
    implementation(project(":user"))
    implementation(project(":utils"))

    implementation(Conf.Deps.SPOCK_CORE)
    implementation(Conf.Deps.SPOCK_SPRING)

    implementation(Conf.Deps.ARCHUNIT)
    implementation(Conf.Deps.GSON)
    implementation(Conf.Deps.JACKSON_DATAFORMAT_YAML)
    implementation(Conf.Deps.JACKSON_MODULE_KOTLIN)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_AMQP)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_TEST)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_WEB)
    implementation(Conf.Deps.SPRING_FRAMEWORK_TEST)
    implementation(Conf.Deps.SPRING_SECURITY_CONFIG)
    implementation(Conf.Deps.SPRING_SECURITY_CORE)
    implementation(Conf.Deps.TESTCONTAINERS)
    implementation(Conf.Deps.TESTCONTAINERS_RABBITMQ)
    implementation(Conf.Deps.TESTCONTAINERS_SPOCK)
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.TEST
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + Conf.Modules.TEST.replace("-", "/")) {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}