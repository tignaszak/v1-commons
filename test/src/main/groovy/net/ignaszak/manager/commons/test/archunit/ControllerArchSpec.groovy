package net.ignaszak.manager.commons.test.archunit

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.core.domain.JavaMethod
import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.lang.ArchCondition
import com.tngtech.archunit.lang.ConditionEvents
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RestController
import spock.lang.Ignore
import spock.lang.Specification

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods

class ControllerArchSpec extends Specification {

    private static final JavaClasses IMPORTED_CLASSES = new ClassFileImporter().importPackages("net.ignaszak.manager")

    def controllerNameRule() {
        given:
        def rule = classes()
                .that()
                .areAnnotatedWith(RestController.class)
                .or()
                .areAnnotatedWith(Controller.class)
                .should()
                .haveSimpleNameEndingWith("Controller")

        expect:
        rule.check(IMPORTED_CLASSES)
    }

    @Ignore
    def controllerShouldReturnResponseRule() {
//        given:
//        def rule = methods()
//                .that()
//                .areDeclaredInClassesThat()
//                .areAnnotatedWith(RestController.class)
//                .should(new ArchCondition<>("return response") {
//                    @Override
//                    public void check(JavaMethod item, ConditionEvents events) {
//
//                    }
//                })
//
//        expect:
//        rule.check(IMPORTED_CLASSES)
    }
}
