package net.ignaszak.manager.commons.test.helper

import net.ignaszak.manager.commons.jwt.JwtConstants
import net.ignaszak.manager.commons.jwt.to.JwtTO
import net.ignaszak.manager.commons.jwt.to.JwtUserTO

import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_EMAIL
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static net.ignaszak.manager.commons.test.helper.UserHelper.anyUserRoles

final class JwtHelper {
    static JwtTO anyJwtTO() {
        new JwtTO(anyJwtUserTO(), anyUserRoles())
    }

    static JwtUserTO anyJwtUserTO() {
        new JwtUserTO(ANY_PUBLIC_ID, ANY_EMAIL)
    }

    static String anyToken() {
        JwtConstants.TOKEN_PREFIX + "eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJpc3MiOiJzZWN1cmUtYXBpIiwiYXVkIjoic2Vjd" +
                "XJlLWFwcCIsInN1YiI6IntcInB1YmxpY0lkXCI6XCI0YzFjNDlkNS1jZTM4LTRhNmItOGNiYi00ZDNiODExYzI4N2VcIixcImVtY" +
                "WlsXCI6XCJ0ZXN0MkBibGEuY29tXCJ9IiwiZXhwIjoxNjE3MTMzMjM4LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.oZug9ri-YddKg" +
                "ysu8jhpc9tkPA60F5LIRMz2uCRCcW4gs9y6_kZx40FbbKQXEfv9ib6JrCMQiYQDVRyl1Cg8LQ"
    }

    private JwtHelper() {}
}
