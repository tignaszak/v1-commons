package net.ignaszak.manager.commons.test.specification.integration.auth

import org.springframework.http.HttpHeaders

class DefaultAuthentication implements Authentication {

    private final headers = new HttpHeaders()

    @Override
    HttpHeaders getHeaders() {
        headers
    }
}
