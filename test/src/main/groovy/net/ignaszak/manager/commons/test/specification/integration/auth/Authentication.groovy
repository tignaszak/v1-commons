package net.ignaszak.manager.commons.test.specification.integration.auth

import org.springframework.http.HttpHeaders

interface Authentication {
    HttpHeaders getHeaders()
}