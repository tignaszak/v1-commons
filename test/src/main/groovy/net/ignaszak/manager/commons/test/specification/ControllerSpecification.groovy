package net.ignaszak.manager.commons.test.specification

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.jwt.exception.JwtException
import net.ignaszak.manager.commons.test.ManagerPropertiesTest
import org.spockframework.spring.SpringBean
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyJwtTO
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.test.helper.UserHelper.USER_ROLE
import static net.ignaszak.manager.commons.test.helper.UserHelper.anyUserPrincipalTO

@TestPropertySource("classpath:application-test.yml")
@ActiveProfiles("test")
abstract class ControllerSpecification extends Specification {

    @Autowired
    protected MockMvc mvc

    protected ManagerProperties managerProperties = ManagerPropertiesTest.getManagerProperties()

    @SpringBean
    protected RabbitTemplate rabbitTemplateMock = Mock()

    @SpringBean
    protected JwtService jwtServiceMock = Mock()

    def setup() {
        jwtServiceMock.getJwtTO(_ as String) >> {
            def token = it[0]
            if (token == anyToken()) {
                return anyJwtTO()
            } else {
                throw new JwtException("Invalid JWT token!")
            }
        }

        def authorities = List.of(new SimpleGrantedAuthority(USER_ROLE))
        def authentication = new UsernamePasswordAuthenticationToken(anyUserPrincipalTO(), null, authorities)

        SecurityContextHolder.getContext().setAuthentication(authentication)
    }

    def cleanup() {
        SecurityContextHolder.clearContext()
    }
}
