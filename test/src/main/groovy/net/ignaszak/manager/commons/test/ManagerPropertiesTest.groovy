package net.ignaszak.manager.commons.test

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import net.ignaszak.manager.commons.conf.properties.ManagerProperties

class ManagerPropertiesTest {

    static final ManagerProperties getManagerProperties() {
        ManagerPropertiesTest.class.classLoader.getResource("application-test.yml").withInputStream {
            def objectMapper = new ObjectMapper(new YAMLFactory())
            objectMapper.registerModule(new KotlinModule())
            def applicationTestProperties = objectMapper.readValue(it, ApplicationTestProperties)
            return applicationTestProperties.manager
        }
    }
}
