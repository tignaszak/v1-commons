package net.ignaszak.manager.commons.test

import net.ignaszak.manager.commons.error.Error
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptor
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptorImpl
import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import org.spockframework.util.Assert
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher

import java.nio.charset.Charset
import java.util.stream.Collectors

import static java.nio.charset.StandardCharsets.UTF_8
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

final class TestUtils {

    private static final ErrorDescriptor errorDescriptor = new ErrorDescriptorImpl()

    private static final CONTENT_TYPE_AND_ENCODING_MATCHER = {
        MvcResult resultMatcher ->
            (
                    APPLICATION_JSON_VALUE.equalsIgnoreCase(resultMatcher.request.contentType)
                    && UTF_8.toString().equalsIgnoreCase(resultMatcher.request.characterEncoding)
            ) || APPLICATION_JSON_UTF8_VALUE.equalsIgnoreCase(resultMatcher.request.contentType)
    }

    static String toString(ResultActions resultActions) {
        return resultActions
                .andReturn()
                .getResponse()
                .getContentAsString(Charset.forName(UTF_8.toString()))
    }

    static boolean assertResponse(ResultActions resultActions, ResultMatcher status, Object response) {
        Objects.requireNonNull(resultActions, "Result actions could nt be null!")
        Objects.requireNonNull(status, "Result matcher could nt be null!")
        Objects.requireNonNull(response, "Response could nt be null!")

        resultActions
                .andExpect(CONTENT_TYPE_AND_ENCODING_MATCHER)
                .andExpect(status)

        def resultString = toString(resultActions)
        def responseString = toJson(response)

        Assert.that(responseString == resultString)

        return true
    }

    static boolean assertNoContentResponse(ResultActions resultActions, ResultMatcher status) {
        Objects.requireNonNull(resultActions, "Result actions could nt be null!")

        resultActions
                .andExpect(CONTENT_TYPE_AND_ENCODING_MATCHER)
                .andExpect(status)

        def resultString = toString(resultActions)

        Assert.that(resultString == null || resultString.isEmpty())

        return true
    }

    static boolean assertNoContentResponse(ResultActions resultActions) {
        return assertNoContentResponse(resultActions, status().isNoContent())
    }

    static void assertErrorResponse(ResultActions resultActions, ResultMatcher status, Error... errors) {
        Objects.requireNonNull(errors, "Error object should not be null!")

        def errorInfoSet = errorDescriptor.getErrors(new TestErrorException(errors))
            .stream()
            .map {
                errorTO -> new ErrorInfoResponse(errorTO.code, errorTO.message)
            }
            .collect(Collectors.toUnmodifiableSet())
        def errorResponse = new ErrorResponse(errorInfoSet)

        resultActions
                .andExpect(CONTENT_TYPE_AND_ENCODING_MATCHER)
                .andExpect(status)
                .andExpect(content().json(toJson(errorResponse)))
    }

    static boolean assertOnePageSize(ResponseEntity<ListResponse<Map<String, String>>> entity, int size) {
        entity.body.list.size() == entity.body.page.totalElements && entity.body.page.totalElements == size
    }

    private TestUtils() {}

    private static class TestErrorException extends ErrorException {
        TestErrorException(Error... errors) {
            super(Set.of(errors))
        }
    }
}
