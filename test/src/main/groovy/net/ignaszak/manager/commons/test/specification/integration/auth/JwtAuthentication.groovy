package net.ignaszak.manager.commons.test.specification.integration.auth

import net.ignaszak.manager.commons.jwt.JwtServiceImpl
import net.ignaszak.manager.commons.jwt.to.JwtUserTO
import org.springframework.http.HttpHeaders

import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_EMAIL
import static net.ignaszak.manager.commons.test.helper.UserHelper.ANY_PUBLIC_ID
import static org.springframework.http.MediaType.APPLICATION_JSON

class JwtAuthentication implements Authentication {

    static final String USER_PUBLIC_ID = ANY_PUBLIC_ID
    static final String USER_EMAIL = ANY_EMAIL

    private final HttpHeaders headers

    JwtAuthentication(String jwtSecret, Long jwtExpirationMs) {
        def jwt = new JwtServiceImpl(jwtExpirationMs, jwtSecret)
                .buildToken(new JwtUserTO(ANY_PUBLIC_ID, ANY_EMAIL), Set.of())
        headers = new HttpHeaders()
        headers.setContentType(APPLICATION_JSON)
        headers.add("Authorization", jwt)
    }

    @Override
    HttpHeaders getHeaders() {
        headers
    }
}
