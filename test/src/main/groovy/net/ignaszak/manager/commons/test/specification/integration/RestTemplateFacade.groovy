package net.ignaszak.manager.commons.test.specification.integration

import org.springframework.http.ResponseEntity

interface RestTemplateFacade {
    ResponseEntity<Object> post(String url, Object request)
    ResponseEntity<Object> get(String url)
    ResponseEntity<Object> put(String url, Object request)
    ResponseEntity<Object> delete(String url)
}