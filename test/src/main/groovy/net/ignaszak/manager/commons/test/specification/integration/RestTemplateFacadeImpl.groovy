package net.ignaszak.manager.commons.test.specification.integration

import net.ignaszak.manager.commons.test.specification.integration.auth.Authentication
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity

import static org.springframework.http.HttpMethod.*

class RestTemplateFacadeImpl implements RestTemplateFacade {

    private final TestRestTemplate restTemplate
    private final int port
    private final Authentication authentication

    RestTemplateFacadeImpl(TestRestTemplate restTemplate,
                           int port,
                           Authentication authentication) {
        this.restTemplate = restTemplate
        this.port = port
        this.authentication = authentication
    }

    @Override
    ResponseEntity<Object> post(String url, Object request) {
        def httpEntity = buildAuthenticatedHttpEntity(request)
        restTemplate.postForEntity(buildUrl(url), httpEntity, Object.class)
    }

    @Override
    ResponseEntity<Object> get(String url) {
        def httpEntity = buildAuthenticatedHttpEntity()
        restTemplate.exchange(buildUrl(url), GET, httpEntity, Object.class)
    }

    @Override
    ResponseEntity<Object> put(String url, Object request) {
        def httpEntity = buildAuthenticatedHttpEntity(request)
        restTemplate.exchange(buildUrl(url), PUT, httpEntity, Object.class)
    }

    @Override
    ResponseEntity<Object> delete(String url) {
        def httpEntity = buildAuthenticatedHttpEntity()
        restTemplate.exchange(buildUrl(url), DELETE, httpEntity, Object.class)
    }

    private HttpEntity buildAuthenticatedHttpEntity() {
        buildAuthenticatedHttpEntity(null)
    }

    private HttpEntity buildAuthenticatedHttpEntity(Object request) {
        new HttpEntity(request, authentication.headers)
    }

    private String buildUrl(String url) {
        "http://localhost:${port}${url}"
    }
}
