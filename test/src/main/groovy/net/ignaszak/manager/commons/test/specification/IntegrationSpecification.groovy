package net.ignaszak.manager.commons.test.specification

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import net.ignaszak.manager.commons.conf.properties.JmsProperties.QueueProperties
import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.error.Error
import net.ignaszak.manager.commons.rest.page.PageResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.rest.response.ListResponse
import net.ignaszak.manager.commons.test.specification.integration.RestTemplateFacade
import net.ignaszak.manager.commons.test.specification.integration.RestTemplateFacadeImpl
import net.ignaszak.manager.commons.test.specification.integration.auth.DefaultAuthentication
import net.ignaszak.manager.commons.test.specification.integration.auth.JwtAuthentication
import net.ignaszak.manager.commons.utils.JsonUtils
import org.springframework.amqp.utils.SerializationUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.GenericContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

import java.util.stream.Collectors
import java.util.stream.Stream

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.http.HttpStatus.NO_CONTENT

@Testcontainers
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
abstract class IntegrationSpecification extends Specification {

    @Shared
    private static final GenericContainer RABBIT_CONTAINER = new GenericContainer("rabbitmq:3-management")
            .withExposedPorts(5672, 15672)

    @LocalServerPort
    protected int port

    @Autowired
    protected ManagerProperties managerProperties

    @Autowired
    protected TestRestTemplate restTemplate

    protected RestTemplateFacade restTemplateAuthFacade
    protected RestTemplateFacade restTemplateFacade

    def setup() {
        if (restTemplateAuthFacade == null) {
            restTemplateAuthFacade = new RestTemplateFacadeImpl(
                    restTemplate,
                    port,
                    new JwtAuthentication(managerProperties.jwt.secret, managerProperties.jwt.expirationMs)
            )
            restTemplateFacade = new RestTemplateFacadeImpl(restTemplate, port, new DefaultAuthentication())
        }
    }

    def cleanupSpec() {
        RABBIT_CONTAINER.stop()
    }

    @DynamicPropertySource
    private static void rabbitmqProperties(DynamicPropertyRegistry registry) {
        RABBIT_CONTAINER.start()

        registry.add("spring.rabbitmq.host", RABBIT_CONTAINER::getContainerIpAddress)
        registry.add("spring.rabbitmq.port", () -> RABBIT_CONTAINER.getMappedPort(5672))
    }

    protected static void assertNoContent(ResponseEntity<Object> responseEntity) {
        assert responseEntity.statusCode == NO_CONTENT
        assert responseEntity.body == null
    }

    protected static void assertErrorResponse(ResponseEntity<Object> responseEntity, HttpStatus statusCode, Error... errors) {
        def response = responseEntity as ResponseEntity<ErrorResponse>

        assert response.statusCode == statusCode
        assert response.body.errors.size() == errors.size()

        def codes = Stream.of(errors)
                .map(Error::getCode)
                .collect(Collectors.toSet())
        assert response.body.errors.stream()
                .map(e -> e.code)
                .collect(Collectors.toSet())
                .containsAll(codes)
    }

    protected static PagingAssertion pagingAssertion(ResponseEntity<Object> responseEntity) {
        new PagingAssertion(responseEntity)
    }

    protected static void assertPaging(ResponseEntity<Object> responseEntity) {
        pagingAssertion(responseEntity).isValid()
    }

    protected static class PagingAssertion {
        private int number = 1
        private int size = 20
        private int totalPages = 1
        private int totalElements = 1

        private ResponseEntity<Object> responseEntity

        PagingAssertion(ResponseEntity<Object> responseEntity) {
            this.responseEntity = responseEntity
        }

        void isValid() {
            PageResponse page

            if (responseEntity.body instanceof ListResponse) {
                page = (PageResponse) responseEntity.body["page"]
            } else {
                page = JsonUtils.toObject((String) responseEntity.body["page"], PageResponse.class)
            }

            assert page != null
            assert page.number == number
            assert page.size == size
            assert page.totalPages == totalPages
            assert page.totalElements == totalElements
        }

        PagingAssertion withNumber(int number) {
            this.number = number
            return this
        }

        PagingAssertion withSize(int size) {
            this.size = size
            return this
        }

        PagingAssertion withTotalPages(int totalPages) {
            this.totalPages = totalPages
            return this
        }

        PagingAssertion withTotalElements(int totalElements) {
            this.totalElements = totalElements
            return this
        }
    }

    protected static Connection newRabbitMqConnection() {
        def factory = new ConnectionFactory()
        factory.setHost(RABBIT_CONTAINER.getContainerIpAddress())
        factory.setPort(RABBIT_CONTAINER.getMappedPort(5672))
        factory.newConnection()
    }

    protected static Channel newQueue(Connection connection, QueueProperties queueProperties) {
        def channel = connection.createChannel()

        channel.exchangeDeclare(queueProperties.exchange, "direct", true)
        channel.queueDeclare(queueProperties.queue, true, false, false, null)
        channel.queueBind(queueProperties.queue, queueProperties.exchange, queueProperties.routingKey)

        channel.queuePurge(queueProperties.queue)

        return channel
    }

    protected static Object getMessage(Channel channel, QueueProperties queueProperties) {
        def response
        def counter = 0

        do {
            counter++
            Thread.sleep(500)
            response = channel.basicGet(queueProperties.queue, false)

        } while (counter <= 5 && response == null)

        SerializationUtils.deserialize(response.getBody())
    }
}
