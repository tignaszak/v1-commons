package net.ignaszak.manager.commons.test.helper

import net.ignaszak.manager.commons.user.to.UserPrincipalTO

final class UserHelper {

    public static final String USER_ROLE = "USER"
    public static final String ANY_EMAIL = "user@email.com"
    public static final String ANY_PUBLIC_ID = "da3da28b-ab50-4929-910c-afd750d2a453"

    static UserPrincipalTO anyUserPrincipalTO() {
        return new UserPrincipalTO(ANY_PUBLIC_ID, ANY_EMAIL)
    }

    static Set<String> anyUserRoles() {
        return Set.of(USER_ROLE)
    }

    private UserHelper() {}
}
