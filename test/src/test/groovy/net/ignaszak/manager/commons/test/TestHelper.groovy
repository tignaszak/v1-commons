package net.ignaszak.manager.commons.test

import net.ignaszak.manager.commons.utils.JsonUtils
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultHandler
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.web.servlet.FlashMap
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView

import java.nio.charset.StandardCharsets

final class TestHelper {

    static ResultActions createMocResultActions(Object model) {
        Objects.requireNonNull(model, "Model could not be null!")

        def mockResponse = new MockHttpServletResponse()
        mockResponse.setContentType(MediaType.APPLICATION_JSON_VALUE)
        mockResponse.setCharacterEncoding(StandardCharsets.UTF_8.toString())
        mockResponse.getWriter().print(JsonUtils.toJson(model))

        def mvcResult = createMockMvcResult(new MockHttpServletRequest(), mockResponse)

        return new ResultActions() {
            @Override
            ResultActions andExpect(ResultMatcher matcher) throws Exception {
                return this
            }

            @Override
            ResultActions andDo(ResultHandler handler) throws Exception {
                return this
            }

            @Override
            MvcResult andReturn() {
                return mvcResult
            }
        }
    }

    static MvcResult createMockMvcResult(MockHttpServletRequest mockRequest, MockHttpServletResponse mockResponse) {
        return new MvcResult() {
            @Override
            MockHttpServletRequest getRequest() {
                return mockRequest
            }

            @Override
            MockHttpServletResponse getResponse() {
                return mockResponse
            }

            @Override
            Object getHandler() {
                return null
            }

            @Override
            HandlerInterceptor[] getInterceptors() {
                return new HandlerInterceptor[0]
            }

            @Override
            ModelAndView getModelAndView() {
                return null
            }

            @Override
            Exception getResolvedException() {
                return null
            }

            @Override
            FlashMap getFlashMap() {
                return null
            }

            @Override
            Object getAsyncResult() {
                return null
            }

            @Override
            Object getAsyncResult(long timeToWait) {
                return null
            }
        }
    }
}
