package net.ignaszak.manager.commons.test

import org.spockframework.util.InternalSpockError
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Specification

import java.time.LocalDateTime

import static java.time.LocalDateTime.now

class TestUtilsSpec extends Specification {

    def "should succeed assertion when objects are equals"() {
        given:
        def model = new Model(1, now())
        def response = new Response(model, List.of(model))

        when:
        def resultActions = TestHelper.createMocResultActions(response)

        then:
        TestUtils.assertResponse(resultActions, MockMvcResultMatchers.status().isOk(), response)
    }

    def "should fail on assertion when objects differs"() {
        given:
        def model1 = new Model(1, now())
        def model2 = new Model(2, now())

        when:
        def resultActions = TestHelper.createMocResultActions(model1)
        TestUtils.assertResponse(resultActions, MockMvcResultMatchers.status().isOk(), model2)

        then:
        thrown(InternalSpockError)
    }

    def "should fail on assertion when objects are null"() {
        given:
        def model = new Model(1, now())

        when:
        def resultActions = TestHelper.createMocResultActions(model)
        TestUtils.assertResponse(resultActions, MockMvcResultMatchers.status().isOk(), null)

        then:
        thrown(NullPointerException)
    }

    class Response {
        Model model
        List<Model> modelList

        Response(Model model, List<Model> modelList) {
            this.model = model
            this.modelList = modelList
        }
    }

    class Model {
        int value
        LocalDateTime localDateTime

        Model(int value, LocalDateTime localDateTime) {
            this.value = value
            this.localDateTime = localDateTime
        }
    }
}
