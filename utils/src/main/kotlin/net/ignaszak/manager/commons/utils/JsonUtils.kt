package net.ignaszak.manager.commons.utils

import com.google.gson.*
import java.lang.reflect.Type
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

object JsonUtils {

    private val gson = GsonBuilder()
        .registerTypeAdapter(LocalDateTime::class.java, LocalDateAdapter())
        .create()

    @JvmStatic
    fun <T> toObject(json: String, clazz: Class<T>): T {
        return gson.fromJson(json, clazz)
    }

    @JvmStatic
    fun toJson(obj: Any): String {
        return gson.toJson(obj)
    }

    private class LocalDateAdapter : JsonSerializer<LocalDateTime?>, JsonDeserializer<LocalDateTime?> {
        override fun serialize(localDateTime: LocalDateTime?,
                               type: Type?,
                               jsonSerializationContext: JsonSerializationContext?): JsonElement {
            return JsonPrimitive(localDateTime!!.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
        }

        override fun deserialize(json: JsonElement,
                                 typeOfT: Type?,
                                 context: JsonDeserializationContext?): LocalDateTime = try {
            LocalDateTime.parse(json.asString, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        } catch (e: DateTimeParseException) {
            LocalDateTime.of(
                LocalDate.parse(json.asString, DateTimeFormatter.ISO_LOCAL_DATE),
                LocalTime.MIN
            )
        }
    }
}