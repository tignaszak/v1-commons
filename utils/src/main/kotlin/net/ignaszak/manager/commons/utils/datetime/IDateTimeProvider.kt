package net.ignaszak.manager.commons.utils.datetime

import java.time.LocalDateTime

interface IDateTimeProvider {
    fun getNow(): LocalDateTime
}