package net.ignaszak.manager.commons.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object ObjectUtils {

    private val gson = Gson()

    @JvmStatic
    fun toStringMap(model: Any) : Map<String, String> {
        val json = gson.toJson(model)

        return gson.fromJson(json, object: TypeToken<Map<String, String>>() {}.type)
    }
}