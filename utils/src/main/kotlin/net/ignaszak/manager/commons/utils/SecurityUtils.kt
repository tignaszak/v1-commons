package net.ignaszak.manager.commons.utils

import java.util.regex.Pattern

object SecurityUtils {

    // Email validation permitted by RFC 5322
    private val EMAIL_PATTERN = Pattern.compile("^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")

    // Minimum eight characters, at least one letter and one digit
    private val PASSWORD_PATTERN = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")

    @JvmStatic
    fun isNotEmail(value: String?): Boolean {
        return value == null || value.isBlank() || !EMAIL_PATTERN.matcher(value).find()
    }

    @JvmStatic
    fun isEmail(value: String?): Boolean {
        return !isNotEmail(value)
    }

    @JvmStatic
    fun isNotPassword(value: String?): Boolean {
        return value == null || value.isBlank() || !PASSWORD_PATTERN.matcher(value).find()
    }

    @JvmStatic
    fun isPassword(value: String?): Boolean {
        return !isNotPassword(value)
    }
}