package net.ignaszak.manager.commons.utils.datetime

import java.time.LocalDateTime

class DateTimeProviderImpl(private val now: LocalDateTime) : IDateTimeProvider {
    override fun getNow() = now
}