package net.ignaszak.manager.commons.utils

import spock.lang.Specification

class SecurityUtilsSpec extends Specification {

    def "test string is not an email"(String email) {
        when:
        def result = SecurityUtils.isNotEmail(email)

        then:
        result

        where:
        email           | _
        null            | _
        ""              | _
        "invalid@email" | _
        "test"          | _
        "test.com"      | _
        "@test"         | _
        "test@"         | _
        ".test"         | _
    }

    def "test string is not an password"(String password) {
        when:
        def result = SecurityUtils.isNotPassword(password)

        then:
        result

        where:
        password       | _
        null           | _
        ""             | _
        "asd1"         | _
        "asdfghjklhjg" | _
        "123654789654" | _
    }
}
