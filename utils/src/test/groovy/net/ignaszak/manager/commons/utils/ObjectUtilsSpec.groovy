package net.ignaszak.manager.commons.utils

import spock.lang.Specification

class ObjectUtilsSpec extends  Specification {

    class SampleObject {

        private String textData
        private Integer integerData
        private Boolean booleanData

        SampleObject(String textData, Integer integerData, Boolean booleanData) {
            this.textData = textData
            this.integerData = integerData
            this.booleanData = booleanData
        }
    }

    def "convert object to map" () {
        given:
        def object = new SampleObject("text", 123, true)

        when:
        def map = ObjectUtils.toStringMap(object)

        then:
        map.get("textData") == "text"
        map.get("integerData") == "123"
        map.get("booleanData") == "true"
    }
}
