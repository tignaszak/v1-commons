package net.ignaszak.manager.commons.utils

import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

class JsonUtilsSpec extends Specification {

    def "serialize LocalDateTime to string"() {
        given:
        def now = LocalDateTime.now()
        def model = new Model(now)

        when:
        def result = JsonUtils.toJson(model)

        then:
        result == String.format("{\"localDateTime\":\"%s\"}", now)
    }

    def "serialize LocalDate to string"() {
        given:
        def now = LocalDate.now()

        when:
        def result = JsonUtils.toObject(String.format("{\"localDateTime\":\"%s\"}", now), Model.class)

        then:
        result.localDateTime.toString() == "${now}T00:00"
    }

    class Model {
        LocalDateTime localDateTime

        Model(LocalDateTime localDateTime) {
            this.localDateTime = localDateTime
        }
    }
}
