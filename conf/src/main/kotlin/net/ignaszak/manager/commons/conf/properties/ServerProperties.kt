package net.ignaszak.manager.commons.conf.properties

data class ServerProperties(
    val pathPrefix: String,
    val gatewayUri: String,
    val gatewayExternalUri: String?,
    val mappings: Set<Mapping>?,
    val pageSize: Int = 20
) {
    data class Mapping(val path: String, val type: String, val scope: Set<String>?)
}
