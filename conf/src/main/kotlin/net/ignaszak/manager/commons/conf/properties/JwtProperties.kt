package net.ignaszak.manager.commons.conf.properties

data class JwtProperties(val secret: String, val expirationMs: Long)
