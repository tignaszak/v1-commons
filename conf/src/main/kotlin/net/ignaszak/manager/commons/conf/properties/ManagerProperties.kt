package net.ignaszak.manager.commons.conf.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "manager")
data class ManagerProperties(
    val jwt: JwtProperties,
    val jms: JmsProperties,
    val email: EmailProperties,
    val oAuth2: OAuth2Properties,
    val server: ServerProperties
)
