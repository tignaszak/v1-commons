package net.ignaszak.manager.commons.conf.properties

data class JmsProperties(val queues: Map<String, QueueProperties>) {
    data class QueueProperties(val queue: String, val exchange: String, val routingKey: String)
}
