package net.ignaszak.manager.commons.conf.properties

data class OAuth2Properties(
    val tokenEndpoint: String,
    val clients: Map<String, CredentialProperties>) {
    data class CredentialProperties(val id: String, val secret: String, val scope: Set<String>)
}
