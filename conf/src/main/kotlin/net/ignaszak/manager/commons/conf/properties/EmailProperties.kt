package net.ignaszak.manager.commons.conf.properties

data class EmailProperties(val from: String)
