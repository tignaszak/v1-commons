plugins {
    `maven-publish`
}

dependencies {
    implementation(Conf.Deps.SPRING_BOOT)
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.CONF
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + Conf.Modules.CONF.replace("-", "/")) {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}