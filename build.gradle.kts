import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version Conf.Versions.KOTLIN
    kotlin("kapt") version Conf.Versions.KOTLIN
}

java {
    sourceCompatibility = JavaVersion.VERSION_18
    targetCompatibility = JavaVersion.VERSION_18
}

allprojects {
    group = Conf.GROUP
    version = Conf.VERSION

    repositories {
        mavenCentral()
    }

    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "java")
    apply(plugin = "groovy")

    allProjectDeps()

    task<Jar>("sourceJar") {
        from(sourceSets.main.get().allSource)
        archiveClassifier.set("sources")
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf(
                "-Xjsr305=strict",
                "-Xjvm-default=all"
            )
            jvmTarget = Conf.Versions.JAVA
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()

        testLogging {
            events("passed", "skipped", "failed")
            exceptionFormat = FULL
            showCauses = true
            showExceptions = true
            showStackTraces = true
        }

        afterSuite(KotlinClosure2<TestDescriptor, TestResult, Unit>({desc, result ->
            if (desc.parent != null) {
                val output = result.run {
                    "${desc.name} results: $resultType ($testCount tests, $successfulTestCount successes, " +
                            "$failedTestCount failures, $skippedTestCount skipped)"
                }
                println(output)
            }
            Unit
        }))
    }
}

tasks.register("publishAll") {
    group = "publishing"
    description = "Publish all modules"
    dependsOn(tasks.build)

    subprojects.forEach {s ->
        if (s.tasks.findByName("publish") != null) {
            dependsOn(s.tasks["publish"])
        }
    }
}