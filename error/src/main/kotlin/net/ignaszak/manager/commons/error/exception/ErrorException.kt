package net.ignaszak.manager.commons.error.exception

import net.ignaszak.manager.commons.error.Error
import java.util.stream.Collectors

abstract class ErrorException(val errorSet: Set<Error>, cause: Throwable?) : RuntimeException(cause) {
    constructor(errorSet: Set<Error>) : this(errorSet, null)
    constructor(error: Error, cause: Throwable) : this(setOf(error), cause)
    constructor(error: Error) : this(setOf(error), null)

    override val message: String
        get() = errorSet.stream()
            .map { obj: Error -> obj.toString() }
            .collect(Collectors.joining(", "))
}