package net.ignaszak.manager.commons.error

enum class AuthError(override val code: String, override val message: String) : Error {
    AUT_AUTHENTICATION("AUT-1", "Could not authenticate user!"),
    AUT_INVALID_TOKEN("AUT-2", "Invalid authentication token!"),
    AUT_TOKEN_EXISTS("AUT-3", "Authentication token already exists!"),
    AUT_AUTH_ERROR("AUT-4", "Could not authenticate user!");

    override fun toString(): String = getString()
}