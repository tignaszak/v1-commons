package net.ignaszak.manager.commons.error.descriptor

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.error.to.ErrorTO
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.util.*
import java.util.stream.Collectors

class ErrorDescriptorImpl : ErrorDescriptor {
    override fun getHttpStatus(exception: ErrorException): HttpStatus {
        val response: ResponseStatus? = exception.javaClass.getAnnotation(ResponseStatus::class.java)

        return response?.value ?: HttpStatus.BAD_REQUEST
    }

    override fun getErrors(exception: ErrorException): Set<ErrorTO> {
        return exception.errorSet
            .stream()
            .map {
                ErrorTO(it.code, it.message)
            }
            .collect(
                Collectors.toCollection {
                    TreeSet(Comparator.comparing(ErrorTO::code))
                }
            )
    }
}