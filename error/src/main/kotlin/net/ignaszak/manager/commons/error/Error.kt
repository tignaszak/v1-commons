package net.ignaszak.manager.commons.error

interface Error {

    val code: String
    val message: String
    val name: String

    fun getString(): String {
        return "$code: $message"
    }

    enum class Default(override val code: String, override val message: String) : Error {
        UNKNOWN("0", "Unknown error!");

        override fun toString(): String = getString()
    }
}