package net.ignaszak.manager.commons.error.to

data class ErrorTO(val code: String, val message: String)
