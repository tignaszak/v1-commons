package net.ignaszak.manager.commons.error

import net.ignaszak.manager.commons.error.exception.ValidationException
import java.util.*
import kotlin.collections.LinkedHashSet

class ErrorCollectorImpl : ErrorCollector {
    private val errorSet: MutableSet<Error>

    init {
        errorSet = TreeSet(Comparator.comparing(Error::code))
    }

    override fun add(error: Error) {
        errorSet.add(error)
    }

    override fun throwException() {
        if (errorSet.isNotEmpty()) {
            val copy = LinkedHashSet(errorSet)
            errorSet.clear()
            throw ValidationException(copy)
        }
    }


}