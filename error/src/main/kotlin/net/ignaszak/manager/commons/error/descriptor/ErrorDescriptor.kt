package net.ignaszak.manager.commons.error.descriptor

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.error.to.ErrorTO
import org.springframework.http.HttpStatus

interface ErrorDescriptor {
    fun getHttpStatus(exception: ErrorException): HttpStatus
    fun getErrors(exception: ErrorException): Set<ErrorTO>
}