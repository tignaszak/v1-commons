package net.ignaszak.manager.commons.error.descriptor

import net.ignaszak.manager.commons.error.Error
import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.error.to.ErrorTO
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

class ErrorExceptionTestHelper {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    static class AnyErrorExceptionWithStatus extends ErrorException {
        AnyErrorExceptionWithStatus(Error error) {
            super(error)
        }

        AnyErrorExceptionWithStatus(Set<Error> errors) {
            super(errors)
        }
    }

    static class AnyErrorExceptionWithoutStatus extends ErrorException {
        AnyErrorExceptionWithoutStatus(Error error) {
            super(error)
        }
    }

    enum TestError implements Error {
        ERROR_1("ERR-1", "Any test error!"),
        ERROR_2("ERR-2", "Any test error!"),
        ANOTHER_1("ANT-1", "Another test error!")

        private String code
        private String message

        TestError(String code, String message) {
            this.code = code
            this.message = message
        }

        @Override
        String getCode() {
            return code
        }

        @Override
        String getMessage() {
            return message
        }

        @Override
        String getName() {
            return name
        }

        @Override
        String toString() {
            return getString()
        }
    }

    static Set<ErrorTO> getSortedTestErrorResponse() {
        def errors = new LinkedHashSet()
        errors.add(new ErrorTO(
                TestError.ANOTHER_1.code,
                TestError.ANOTHER_1.message
        ))
        errors.add(new ErrorTO(
                TestError.ERROR_1.code,
                TestError.ERROR_1.message
        ))
        errors.add(new ErrorTO(
                TestError.ERROR_2.code,
                TestError.ERROR_2.message
        ))
        return errors
    }
}
