package net.ignaszak.manager.commons.error.descriptor

import org.springframework.http.HttpStatus
import spock.lang.Specification

import static net.ignaszak.manager.commons.utils.JsonUtils.toJson

class ErrorDescriptorSpec extends Specification {

    ErrorDescriptor subject = new ErrorDescriptorImpl()

    def "get http status from error exception"() {
        given:
        def exception = new ErrorExceptionTestHelper.AnyErrorExceptionWithStatus(ErrorExceptionTestHelper.TestError.ERROR_1)

        when:
        def result = subject.getHttpStatus(exception)

        then:
        result == HttpStatus.NOT_FOUND
    }

    def "get default http status from error exception without defined response status"() {
        given:
        def exception = new ErrorExceptionTestHelper.AnyErrorExceptionWithoutStatus(ErrorExceptionTestHelper.TestError.ERROR_1)

        when:
        def result = subject.getHttpStatus(exception)

        then:
        result == HttpStatus.BAD_REQUEST
    }

    def "get sorted errors from error exception"() {
        given:
        def exception = new ErrorExceptionTestHelper.AnyErrorExceptionWithStatus(
                Set.of(
                        ErrorExceptionTestHelper.TestError.ERROR_2,
                        ErrorExceptionTestHelper.TestError.ANOTHER_1,
                        ErrorExceptionTestHelper.TestError.ERROR_1
                )
        )

        when:
        def result = subject.getErrors(exception)

        then:
        toJson(result) == toJson(ErrorExceptionTestHelper.getSortedTestErrorResponse())
    }
}
