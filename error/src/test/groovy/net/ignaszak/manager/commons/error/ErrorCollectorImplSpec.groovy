package net.ignaszak.manager.commons.error

import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.error.exception.ValidationException
import spock.lang.Specification

class ErrorCollectorImplSpec extends Specification {

    ErrorCollector errorCollector

    def setup() {
        this.errorCollector = new ErrorCollectorImpl()
    }

    def "test throw exception after single error is added"() {
        given:
        errorCollector.add(Error.Default.UNKNOWN)

        when:
        errorCollector.throwException()

        then:
        def e = thrown ValidationException
        e.message == Error.Default.UNKNOWN.toString()
    }

    def "test throw exception after many errors are added"() {
        given:
        errorCollector.add(AuthError.AUT_AUTHENTICATION)
        errorCollector.add(Error.Default.UNKNOWN)

        when:
        errorCollector.throwException()

        then:
        def e = thrown ValidationException
        e.message == Error.Default.UNKNOWN.toString() + ", " + AuthError.AUT_AUTHENTICATION.toString()
    }

    def "test throw exception with single error after duplicated errors added"() {
        given:
        errorCollector.add(Error.Default.UNKNOWN)
        errorCollector.add(Error.Default.UNKNOWN)

        when:
        errorCollector.throwException()

        then:
        def e = thrown ValidationException
        e.message == Error.Default.UNKNOWN.toString()
    }

    def "do not throw exception when no error is added"() {
        when:
        errorCollector.throwException()

        then:
        notThrown ValidationException
    }

    def "reset error collection after an exception is thrown"() {
        when:
        List<Integer> counter = new ArrayList<>()

        errorCollector.add(AuthError.AUT_AUTHENTICATION)
        try {
            errorCollector.throwException()
        } catch (ErrorException e) {
            counter.add(e.errorSet.size())
        }

        errorCollector.add(Error.Default.UNKNOWN)
        try {
            errorCollector.throwException()
        } catch (ErrorException e) {
            counter.add(e.errorSet.size())
        }

        then:
        for (def count : counter) {
            assert count == 1
        }
    }
}
