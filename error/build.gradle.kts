plugins {
    `maven-publish`
}

dependencies {
    implementation(Conf.Deps.SPRING_FRAMEWORK_WEB) {
        exclude(group = "org.springframework", module = "spring-core")
        exclude(group = "org.springframework", module = "spring-beans")
    }

    testImplementation(project(":utils"))
    testImplementation(Conf.Deps.GSON)
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.ERROR
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + Conf.Modules.ERROR.replace("-", "/")) {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}