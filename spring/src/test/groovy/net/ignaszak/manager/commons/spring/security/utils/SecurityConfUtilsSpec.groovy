package net.ignaszak.manager.commons.spring.security.utils

import net.ignaszak.manager.commons.jwt.JwtConstants
import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.jwt.exception.JwtException
import net.ignaszak.manager.commons.jwt.to.JwtUserTO
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.config.AuthorizationFilter
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.security.mapper.UserSecurityMapper
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import org.jetbrains.annotations.NotNull
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.TestingAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import spock.lang.Specification

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

import static java.nio.charset.StandardCharsets.UTF_8
import static java.util.Collections.singleton
import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN
import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.ALLOWED
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED_INACTIVE
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyJwtTO
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson

class SecurityConfUtilsSpec extends Specification {

    private AuthenticationManager authenticationManager = new AuthenticationManager() {
        @Override
        Authentication authenticate(Authentication authentication) throws AuthenticationException {
            return new TestingAuthenticationToken(null, null)
        }
    }

    private FilterChain filterChain = new FilterChain() {
        @Override
        void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {}
    }

    private UserSecurityMapper userSecurityMapper = new UserSecurityMapper() {
        @Override
        UserPrincipalTO jwtUserTOToUserPrincipalTO(@NotNull JwtUserTO jwtUserTO) {
            return new UserPrincipalTO("publicId", "user@email.pl")
        }
    }

    private UriMatcher uriValidator = new UriMatcher() {
        @Override
        Mapping match(@NotNull String uri) {
            if (anyAllowedUri().equalsIgnoreCase(uri)) {
                return new Mapping(uri, ALLOWED)
            } else if (anyAuthenticatedInactiveUri().equalsIgnoreCase(uri)) {
                return new Mapping(uri, AUTHENTICATED_INACTIVE)
            }else {
                return new Mapping(uri, AUTHENTICATED)
            }
        }
    }

    private JwtOAuth2Service jwtOAuth2Service = new JwtOAuth2Service() {
        @Override
        Set<String> extractScopes(@NotNull String token) {
            return Set.of()
        }
    }

    private JwtService jwtServiceMock = Mock()

    private BasicAuthenticationFilter subject = new AuthorizationFilter(
            authenticationManager,
            userSecurityMapper,
            jwtServiceMock,
            jwtOAuth2Service,
            uriValidator
    )

    def "should continue request if token is correct and user"() {
        given:
        def anyToken = anyToken()
        def request = anyRequest(anyToken)
        def response = anyResponse()
        1 * jwtServiceMock.getJwtTO(anyToken) >> anyJwtTO()

        when:
        subject.doFilter(request, response, filterChain)

        then:
        response.status == HttpStatus.OK.value()
    }

    def "should continue unauthorized request without token"() {
        given:
        def request = new MockHttpServletRequest()
        request.requestURI = anyAllowedUri()
        def response = anyResponse()
        def subject = new AuthorizationFilter(
                authenticationManager,
                userSecurityMapper,
                jwtServiceMock,
                jwtOAuth2Service,
                uriValidator
        )

        when:
        subject.doFilter(request, response, filterChain)

        then:
        0 * jwtServiceMock.getJwtTO(_)
        response.status == HttpStatus.OK.value()
    }

    def "should return ErrorResponse if token is invalid"() {
        given:
        def invalidToken = invalidToken()
        def request = anyRequest(invalidToken)
        def response = anyResponse()
        1 * jwtServiceMock.getJwtTO(_ as String) >> {
            token -> throw new JwtException("Message", new RuntimeException())
        }

        when:
        subject.doFilter(request, response, filterChain)

        then:
        response.status == HttpStatus.FORBIDDEN.value()
        response.contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)
        response.characterEncoding == UTF_8.displayName()
        response.contentAsString == toJson(authErrorResponse())
    }

    def "should authorized request with inactive user"() {
        given:
        def anyToken = anyToken()
        def request = new MockHttpServletRequest()
        request.requestURI = anyAuthenticatedInactiveUri()
        request.addHeader(TOKEN_HEADER, anyToken)
        def response = anyResponse()
        1 * jwtServiceMock.getJwtTO(anyToken) >> anyJwtTO()
        def subject = new AuthorizationFilter(
                authenticationManager,
                userSecurityMapper,
                jwtServiceMock,
                jwtOAuth2Service,
                uriValidator
        )

        when:
        subject.doFilter(request, response, filterChain)

        then:
        response.status == HttpStatus.OK.value()
    }

    private static String anyAllowedUri() {
        "/any/allowed/uri"
    }

    private static String anyAuthenticatedInactiveUri() {
        "/any/authenticated/inactive/uri"
    }

    private static ErrorResponse authErrorResponse() {
        new ErrorResponse(
                singleton(
                        new ErrorInfoResponse(AUT_INVALID_TOKEN.code, AUT_INVALID_TOKEN.message)
                )
        )
    }

    private static MockHttpServletResponse anyResponse() {
        def response = new MockHttpServletResponse()
        response.status = HttpStatus.OK.value()

        return response
    }

    private static String invalidToken() {
        JwtConstants.TOKEN_PREFIX + "anyToken"
    }

    private static HttpServletRequest anyRequest(String token) {
        def request = new MockHttpServletRequest()
        request.addHeader(TOKEN_HEADER, token)

        return request
    }
}
