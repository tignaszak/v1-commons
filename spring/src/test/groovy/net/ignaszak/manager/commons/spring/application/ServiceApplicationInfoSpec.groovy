package net.ignaszak.manager.commons.spring.application

import org.springframework.context.ApplicationContext
import spock.lang.Specification

class ServiceApplicationInfoSpec extends Specification {

    private ApplicationContext contextMock = Mock()

    def "test found application info" () {
        given:
        1 * contextMock.getId() >> "manager-users-v1.2-service"

        when:
        def subject = new ServiceApplicationInfo(contextMock)

        then:
        "users" == subject.getApplicationName()
        "v1.2" == subject.getApiVersion()
    }

    def "test not found application info" () {
        given:
        1 * contextMock.getId() >> "some unknown application id"

        when:
        def subject = new ServiceApplicationInfo(contextMock)

        then:
        subject.getApplicationName().isBlank()
        subject.getApiVersion().isBlank()
    }
}
