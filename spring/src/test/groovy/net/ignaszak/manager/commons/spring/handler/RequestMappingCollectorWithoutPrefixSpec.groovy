package net.ignaszak.manager.commons.spring.handler

import net.ignaszak.manager.commons.spring.TestController
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest

import static TestUtils.assertMappings

@WebMvcTest(TestController)
class RequestMappingCollectorWithoutPrefixSpec extends ControllerSpecification {

    def "get mappings"() {
        given:
        def repository = RequestMappingRepositorySingleton.INSTANCE

        when: 'Start spring context'

        then:
        assertMappings(repository.findAll(), "/api")
    }
}
