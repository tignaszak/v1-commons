package net.ignaszak.manager.commons.spring.handler

import net.ignaszak.manager.commons.error.Error
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptor
import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.error.to.ErrorTO
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.handler.mapper.ErrorHandlerMapper
import org.jetbrains.annotations.NotNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import java.util.stream.Collectors

class ErrorHandlerSpec extends Specification {

    public static final String ANY_EXCEPTION_MESSAGE = "Any exception message"

    private ErrorDescriptor errorDescriptor = new ErrorDescriptor() {
        @Override
        HttpStatus getHttpStatus(@NotNull ErrorException exception) {
            return HttpStatus.NOT_FOUND
        }

        @Override
        Set<ErrorTO> getErrors(@NotNull ErrorException exception) {
            return Set.of(
                    new ErrorTO(
                            ErrorExceptionTestHelper.TestError.ERROR_1.code,
                            ErrorExceptionTestHelper.TestError.ERROR_1.message
                    )
            )
        }
    }

    private ErrorHandlerMapper errorHandlerMapper = new ErrorHandlerMapper() {
        @Override
        Set<ErrorInfoResponse> errorTOSetToErrorInfoResponseSet(@NotNull Set<ErrorTO> errorTOSet) {
            return errorTOSet.stream()
                    .map {
                        errorTO -> new ErrorInfoResponse(errorTO.code, errorTO.message)
                    }
                    .collect(Collectors.toUnmodifiableSet())
        }
    }

    ErrorHandler subject = new ErrorHandler(errorDescriptor, errorHandlerMapper)

    def "get response entity from error exception"() {
        given:
        def exception = new ErrorExceptionTestHelper.AnyErrorExceptionWithStatus(ErrorExceptionTestHelper.TestError.ERROR_1)

        when:
        ResponseEntity<ErrorResponse> result = subject.handleErrorException(exception)

        then:
        result.statusCode == HttpStatus.NOT_FOUND
        result.getBody() == getTestErrorResponse()
    }

    def "handle other exceptions"() {
        given:
        def exception = new Exception(ANY_EXCEPTION_MESSAGE)

        when:
        ResponseEntity<ErrorResponse> result = subject.handleException(exception)

        then:
        result.statusCode == HttpStatus.BAD_REQUEST
        result.getBody() == getErrorResponseForOtherExceptions()
    }

    private static ErrorResponse getTestErrorResponse() {
        return new ErrorResponse(
                Set.of(
                        new ErrorInfoResponse(
                                ErrorExceptionTestHelper.TestError.ERROR_1.code,
                                ErrorExceptionTestHelper.TestError.ERROR_1.message
                        )
                )
        )
    }

    static ErrorResponse getErrorResponseForOtherExceptions() {
        return new ErrorResponse(
                Set.of(
                        new ErrorInfoResponse(
                                Error.Default.UNKNOWN.code,
                                Error.Default.UNKNOWN.message
                        )
                )
        )
    }
}
