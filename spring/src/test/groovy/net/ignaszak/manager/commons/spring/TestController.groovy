package net.ignaszak.manager.commons.spring

import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter
import net.ignaszak.manager.commons.spring.TestApplication.CatchBean
import net.ignaszak.manager.commons.spring.handler.annotation.AllowedMapping
import net.ignaszak.manager.commons.spring.handler.annotation.AuthenticatedInactiveMapping
import net.ignaszak.manager.commons.spring.handler.annotation.InnerMapping
import net.ignaszak.manager.commons.spring.security.resolver.ParameterFilterResolverSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TestController {

    @Autowired
    private CatchBean catchBean

    @GetMapping("/test")
    DataResponse<String> test() {
        return new DataResponse<>("test")
    }

    @InnerMapping(["scope1", "scope2"])
    @GetMapping("/inner")
    DataResponse<String> inner() {
        return new DataResponse<>("inner")
    }

    @AllowedMapping
    @GetMapping("/allowed")
    DataResponse<String> allowed() {
        return new DataResponse<>("allowed")
    }

    @AuthenticatedInactiveMapping
    @GetMapping("/authenticatedInactive")
    DataResponse<String> authenticatedInactive() {
        return new DataResponse<>("authenticatedInactive")
    }

    @AllowedMapping
    @GetMapping("/testFilter")
    DataResponse<String> testFilter(@ParameterFilter ParameterFilterResolverSpec.FilterRequest filter) {
        catchBean.catchObject(filter)

        return new DataResponse<>("filterTest")
    }
}
