package net.ignaszak.manager.commons.spring.handler.processor.processors

import net.ignaszak.manager.commons.spring.application.ApplicationInfoInterface
import net.ignaszak.manager.commons.spring.handler.processor.MappingProcessorInterface
import net.ignaszak.manager.commons.spring.handler.registry.RegistryServiceInterface
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import spock.lang.Specification

import java.util.stream.Collectors

import static java.util.Collections.emptySet
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.ALLOWED
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED_INACTIVE
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.INNER

class MappingRegistryProcessorSpec extends Specification {
    private static final String APP_NAME = "appName"
    private static final String API_VERSION = "v1"

    private RegistryServiceInterface registryServiceMock = Mock()

    private ApplicationInfoInterface applicationInfo = new ApplicationInfoInterface() {
        @Override
        String getApplicationName() {
            return APP_NAME
        }

        @Override
        String getApiVersion() {
            return API_VERSION
        }
    }

    private MappingProcessorInterface subject = new MappingRegistryProcessor(registryServiceMock, applicationInfo)

    def "process mappings" () {
        given:
        def mappings = Set.of(
                new Mapping("/INNER", INNER),
                new Mapping("/AUTHENTICATED", AUTHENTICATED),
                new Mapping("/AUTHENTICATED_INACTIVE", AUTHENTICATED_INACTIVE),
                new Mapping("/ALLOWED", ALLOWED)
        )

        when:
        subject.process(mappings)

        then:
        1 * registryServiceMock.register(_ as Set<Mapping>) >> {arguments ->
            def resultMappings = (Set<Mapping>) arguments.get(0)
            assert resultMappings.size() == 3
            assert resultMappings.stream()
                    .map(mapping -> "${mapping.path}:${mapping.type}")
                    .collect(Collectors.toList())
                    .containsAll(Set.of(
                            "/${API_VERSION}/${APP_NAME}/AUTHENTICATED_INACTIVE:${AUTHENTICATED_INACTIVE}",
                            "/${API_VERSION}/${APP_NAME}/INNER:${ALLOWED}",
                            "/${API_VERSION}/${APP_NAME}/ALLOWED:${ALLOWED}"
                    ))
        }
    }

    def "do not send empty message" (Set<Mapping> mappings) {
        when:
        subject.process(mappings)

        then:
        0 * registryServiceMock.register(_)

        where:
        mappings                                             | _
        emptySet()                                           | _
        Set.of(new Mapping("/AUTHENTICATED", AUTHENTICATED)) | _
    }
}
