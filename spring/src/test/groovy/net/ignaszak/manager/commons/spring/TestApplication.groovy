package net.ignaszak.manager.commons.spring

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class TestApplication {
    static void main(String[] args) {
        SpringApplication.run(TestApplication, args)
    }

    @Bean
    CatchBean catchBean() {
        return new CatchBean()
    }

    class CatchBean {
        def catchObject(Object object) {
            System.out.println(object)
        }
    }
}
