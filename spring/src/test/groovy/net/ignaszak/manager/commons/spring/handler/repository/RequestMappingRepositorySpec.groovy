package net.ignaszak.manager.commons.spring.handler.repository

import spock.lang.Specification

class RequestMappingRepositorySpec extends Specification {

    private static final RequestMappingRepository subject = RequestMappingRepositorySingleton.INSTANCE

    def "should return all mappings"() {
        setup:
        subject.clear()

        when:
        subject.add(new Mapping("/inner/endpoint", Mapping.Type.INNER, Set.of("scope1", "scope2")))
        subject.add(new Mapping("/allowed/endpoint", Mapping.Type.ALLOWED))

        then:
        subject.findAll().size() == 2
    }
}
