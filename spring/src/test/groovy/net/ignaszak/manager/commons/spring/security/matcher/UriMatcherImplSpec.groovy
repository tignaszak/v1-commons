package net.ignaszak.manager.commons.spring.security.matcher

import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepository
import org.jetbrains.annotations.NotNull
import spock.lang.Specification

import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.ALLOWED
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.INNER

class UriMatcherImplSpec extends Specification {

    private static final requestMappingRepository = new RequestMappingRepository() {

        private Map<String, Mapping> map = Map.of(
                "/inner/uri1", new Mapping("/inner/**", INNER, Set.of("scope1", "scope2")),
                "/allowed/uri1", new Mapping("/allowed/uri1", ALLOWED),
                "/allowed/path/{var}", new Mapping("/allowed/path/{var}", ALLOWED)
        )

        @Override
        void add(@NotNull Mapping mapping) {}

        @Override
        Set<Mapping> findAll() {
            return map.values()
        }

        @Override
        void clear() {
            map.clear()
        }
    }

    def "succeed when inner uri is found" () {
        given:
        def requestUri = "/inner/uri1"
        def subject = new UriMatcherImpl(requestMappingRepository)

        when:
        def result = subject.match(requestUri)

        then:
        result.getType() == INNER
    }

    def "succeed when allowed uri is found" () {
        given:
        def requestUri = "/allowed/uri1"
        def subject = new UriMatcherImpl(requestMappingRepository)

        when:
        def result = subject.match(requestUri)

        then:
        result.getPath() == requestUri
        result.getType() == ALLOWED
    }

    def "succeed when inner uri with prefix is found" () {
        given:
        def requestUri = "/v1/prefix/inner/uri1"
        def prefix = "/v\\d+/prefix"
        def subject = new UriMatcherImpl(requestMappingRepository, prefix)

        when:
        def result = subject.match(requestUri)

        then:
        result.getType() == INNER
    }

    def "succeed when allowed uri with prefix is found" () {
        given:
        def requestUri = "/v1/prefix/allowed/uri1"
        def prefix = "/v\\d+/prefix"
        def subject = new UriMatcherImpl(requestMappingRepository, prefix)

        when:
        def result = subject.match(requestUri)

        then:
        result.getType() == ALLOWED
    }

    def "any request uri not mapped with allowed or inner annotation should be jwt secured" () {
        given:
        def requestUri = "/v1/jwt/uri1"
        def subject = new UriMatcherImpl(requestMappingRepository)

        when:
        def result = subject.match(requestUri)

        then:
        result.getPath() == requestUri
        result.getType() == Mapping.Type.AUTHENTICATED
    }

    def "any request uri with prefix not mapped with allowed or inner annotation should be jwt secured" () {
        given:
        def requestUri = "/v1/prefix1/allowed/uri1"
        def prefix = "/v\\d+/prefix"
        def subject = new UriMatcherImpl(requestMappingRepository, prefix)

        when:
        def result = subject.match(requestUri)

        then:
        result.getPath() == requestUri
        result.getType() == Mapping.Type.AUTHENTICATED
    }

    def "change path variable to regex" () {
        given:
        def requestUri = "/allowed/path/123-456-789"
        def subject = new UriMatcherImpl(requestMappingRepository)

        when:
        def result = subject.match(requestUri)

        then:
        result.getType() == ALLOWED
    }
}
