package net.ignaszak.manager.commons.spring.handler

import net.ignaszak.manager.commons.spring.handler.repository.Mapping

import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.ALLOWED
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED_INACTIVE
import static net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.INNER

class TestUtils {
    static void assertMappings(Set<Mapping> mappings, String prefix) {
        for (def expectingMapping : getExpectedMappings(prefix)) {
            def foundMappingOptional = mappings
                    .stream()
                    .filter {
                        it.path == expectingMapping.path
                    }
                    .findFirst()

            assert foundMappingOptional.isPresent()

            def foundMapping = foundMappingOptional.get()

            assert expectingMapping.type == foundMapping.type

            if (!expectingMapping.scope.isEmpty()) {
                assert !foundMapping.scope.isEmpty() && foundMapping.scope.containsAll(expectingMapping.scope)
            }
        }
    }

    private static Set<Mapping> getExpectedMappings(String prefix) {
        Set.of(
                new Mapping(prefix + "/inner", INNER, Set.of("scope1", "scope2")),
                new Mapping(prefix + "/allowed", ALLOWED),
                new Mapping(prefix + "/authenticatedInactive", AUTHENTICATED_INACTIVE),
                new Mapping("/custom/service/path", INNER, Set.of("test"))
        )
    }
}
