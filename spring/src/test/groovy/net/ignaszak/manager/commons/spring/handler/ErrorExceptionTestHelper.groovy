package net.ignaszak.manager.commons.spring.handler

import net.ignaszak.manager.commons.error.Error
import net.ignaszak.manager.commons.error.exception.ErrorException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

class ErrorExceptionTestHelper {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    static class AnyErrorExceptionWithStatus extends ErrorException {
        AnyErrorExceptionWithStatus(Error error) {
            super(error)
        }
    }

    enum TestError implements Error {
        ERROR_1("ERR-1", "Any test error!"),
        ERROR_2("ERR-2", "Any test error!"),
        ANOTHER_1("ANT-1", "Another test error!")

        private String code
        private String message

        TestError(String code, String message) {
            this.code = code
            this.message = message
        }

        @Override
        String getCode() {
            return code
        }

        @Override
        String getMessage() {
            return message
        }

        @Override
        String getName() {
            return name
        }

        @Override
        String toString() {
            return getString()
        }
    }
}
