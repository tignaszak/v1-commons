package net.ignaszak.manager.commons.spring.conf

import net.ignaszak.manager.commons.error.AuthError
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.spring.config.CommonForbiddenEntryPoint
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import org.springframework.security.core.AuthenticationException
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import spock.lang.Specification

import static java.nio.charset.StandardCharsets.UTF_8
import static java.util.Collections.singleton
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static org.springframework.http.HttpStatus.FORBIDDEN
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE

class CommonForbiddenEntryPointSpec extends Specification {

    public static final String ANY_AUTH_EXCEPTION_MESSAGE = "any auth exception message"

    CommonForbiddenEntryPoint subject = new CommonForbiddenEntryPoint()

    def "return json forbidden response"() {
        given:
        def request = new MockHttpServletRequest()
        def response = new MockHttpServletResponse()
        def exception = anyAuthException()

        when:
        subject.commence(request, response, exception)

        then:
        response.getStatus() == FORBIDDEN.value()
        response.getContentType().startsWith(APPLICATION_JSON_VALUE)
        response.getCharacterEncoding() == UTF_8.displayName()
        response.getContentAsString() == toJson(getAuthErrorResponse())
    }

    private static ErrorResponse getAuthErrorResponse() {
        new ErrorResponse(singleton(new ErrorInfoResponse(AuthError.AUT_AUTHENTICATION.code, AuthError.AUT_AUTHENTICATION.message)))
    }

    private static AuthenticationException anyAuthException() {
        new AuthenticationException(ANY_AUTH_EXCEPTION_MESSAGE) {}
    }
}
