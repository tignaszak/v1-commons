package net.ignaszak.manager.commons.spring.security.resolver

import net.ignaszak.manager.commons.rest.filter.Operator
import net.ignaszak.manager.commons.spring.TestApplication.CatchBean
import net.ignaszak.manager.commons.spring.TestController
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import org.jetbrains.annotations.NotNull
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest

import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

import java.time.LocalDateTime

@WebMvcTest(TestController)
class ParameterFilterResolverSpec extends ControllerSpecification {

    class FilterRequest {
        private final Operator<Set<String>> publicIds
        private final LocalDateTime date

        FilterRequest(@NotNull final Operator<Set<String>> publicIds, @NotNull final LocalDateTime date) {
            this.publicIds = publicIds
            this.date = date
        }
    }

    @SpringBean
    private CatchBean catchBeanMock = Mock()

    def "map filter query"() {
        given:
        def now = LocalDateTime.now()

        when:
        mvc.perform(get("/api/testFilter?filter=publicIds!:[jeden,dwa,trzy]|date:${now}").header(TOKEN_HEADER, anyToken()))

        then:
        1 * catchBeanMock.catchObject({FilterRequest filter ->
            filter.publicIds.type == Operator.Type.NOT_EQUALS
            filter.publicIds.value.size() == 3
            filter.date == now
        })
    }

    def "map filter query with special chars"() {
        given:
        def now = LocalDateTime.now()

        when:
        mvc.perform(get("/api/testFilter?filter=publicIds!:[jeden%25,dwa,tr%23zy,,%20%26%2F%5C]|date:${now}&other=value").header(TOKEN_HEADER, anyToken()))

        then:
        1 * catchBeanMock.catchObject({FilterRequest filter ->
            filter.publicIds.type == Operator.Type.NOT_EQUALS
            filter.publicIds.value.size() == 3
            filter.date == now
        })
    }

    def "map query without filter request"() {
        when:
        mvc.perform(get("/api/testFilter").header(TOKEN_HEADER, anyToken()))

        then:
        1 * catchBeanMock.catchObject({FilterRequest filter ->
            filter.publicIds == null
            filter.date == null
        })
    }
}
