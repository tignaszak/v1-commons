package net.ignaszak.manager.commons.spring.conf

import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.TestController
import net.ignaszak.manager.commons.test.TestUtils
import net.ignaszak.manager.commons.test.specification.ControllerSpecification
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest

import static java.util.Collections.singleton
import static net.ignaszak.manager.commons.error.AuthError.AUT_INVALID_TOKEN
import static net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_HEADER
import static net.ignaszak.manager.commons.test.helper.JwtHelper.anyToken
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(TestController)
class CommonWebSecurityConfigSpec extends ControllerSpecification {

    def "get response from secured endpoint with valid token"() {
        when:
        def request = get("/api/test")
                .header(TOKEN_HEADER, anyToken())

        then:
        TestUtils.assertResponse(mvc.perform(request), status().isOk(), new DataResponse("test"))
    }

    def "get error response from secured endpoint without token"() {
        when:
        def request = get("/api/test")

        then:
        TestUtils.assertResponse(
                mvc.perform(request),
                status().isForbidden(),
                invalidTokenErrorResponse()
        )
    }

    def "get error response from secured endpoint with invalid token"() {
        when:
        def request = get("/api/test")
                .header(TOKEN_HEADER, "invalid token")

        then:
        TestUtils.assertResponse(
                mvc.perform(request),
                status().isForbidden(),
                invalidTokenErrorResponse()
        )
    }

    private static ErrorResponse invalidTokenErrorResponse() {
        new ErrorResponse(singleton(new ErrorInfoResponse(AUT_INVALID_TOKEN.code, AUT_INVALID_TOKEN.message)))
    }
}
