package net.ignaszak.manager.commons.spring.handler.registry

import net.ignaszak.manager.commons.spring.handler.repository.Mapping

interface RegistryServiceInterface {
    fun register(mappings: Set<Mapping>)
}