package net.ignaszak.manager.commons.spring.handler.collector

import net.ignaszak.manager.commons.spring.handler.annotation.AllowedMapping
import net.ignaszak.manager.commons.spring.handler.annotation.AuthenticatedInactiveMapping
import net.ignaszak.manager.commons.spring.handler.annotation.InnerMapping
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type
import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.*
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
import java.lang.Exception
import java.lang.reflect.Method
import java.util.Collections.unmodifiableSet

private const val MANAGER_CLASS_PREFIX = "net.ignaszak.manager"

class AnnotationRequestMappingCollector (
    private val mapping: RequestMappingHandlerMapping,
    private val prefix: String
) : RequestMappingCollector {

    override fun collect() : Set<Mapping> {
        val mappings = mutableSetOf<Mapping>()

        for (method in mapping.handlerMethods) {
            val paths = mutableListOf<String>()
            var type: Type? = null
            var scope: Set<String>? = null

            if (method.value.method.declaringClass.name.startsWith(MANAGER_CLASS_PREFIX)) {
                for (annotation in method.value.method.annotations) {
                    when (annotation) {
                        is AllowedMapping               -> {
                            valid(type, method.value.method)
                            type = ALLOWED
                        }
                        is InnerMapping                 -> {
                            valid(type, method.value.method)
                            type = INNER
                            scope = getScope(annotation.value, method.value.method)
                        }
                        is AuthenticatedInactiveMapping -> {
                            valid(type, method.value.method)
                            type = AUTHENTICATED_INACTIVE
                        }

                        is GetMapping                   -> paths.addAll(annotation.value)
                        is PutMapping                   -> paths.addAll(annotation.value)
                        is PostMapping                  -> paths.addAll(annotation.value)
                        is DeleteMapping                -> paths.addAll(annotation.value)
                        is RequestMapping               -> paths.addAll(annotation.value)
                    }
                }
            }

            if (paths.isNotEmpty()) {
                type = type ?: AUTHENTICATED
                scope = scope ?: setOf()

                paths.forEach {
                    mappings.add(Mapping("${prefix}${it}", type, scope))
                }
            }
        }

        return unmodifiableSet(mappings)
    }

    private fun valid(type: Type?, method: Method) {
        if (type != null) {
            throw Exception("Multiple mapping annotations detected in '$method'")
        }
    }

    private fun getScope(value: Array<out String>?, method: Method) : Set<String> {
        if (value == null || value.isEmpty()) {
            throw Exception("InnerMapping annotation has not defined scope in '$method'")
        }

        return value.toSet()
    }
}