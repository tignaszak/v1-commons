package net.ignaszak.manager.commons.spring.security.mapper

import net.ignaszak.manager.commons.jwt.to.JwtUserTO
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import org.mapstruct.Mapper
import org.springframework.stereotype.Component

@Component
@Mapper(componentModel = "spring")
interface UserSecurityMapper {
    fun jwtUserTOToUserPrincipalTO(jwtUserTO: JwtUserTO): UserPrincipalTO
}