package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.httpclient.client.provider.Oauth2ProviderBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.oauth2.client.*
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import java.util.Collections.emptyList

@Configuration
open class CommonOAuth2ClientConfig {
    @Bean
    open fun auth2AuthorizedClientService(clientRegistrationRepository: ClientRegistrationRepository): OAuth2AuthorizedClientService {
        return InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository)
    }

    @Bean
    open fun authorizedClientServiceAndManager(
        clientRegistrationRepository: ClientRegistrationRepository,
        authorizedClientService: OAuth2AuthorizedClientService
    ): AuthorizedClientServiceOAuth2AuthorizedClientManager {
        val authorizedClientProvider = OAuth2AuthorizedClientProviderBuilder.builder()
            .clientCredentials()
            .build()

        val authorizedClientManager = AuthorizedClientServiceOAuth2AuthorizedClientManager(
            clientRegistrationRepository, authorizedClientService
        )

        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider)

        return authorizedClientManager
    }

    @Bean
    open fun oAuth2ProviderBuilder(
        authorizedClientServiceAndManager: AuthorizedClientServiceOAuth2AuthorizedClientManager
    ) : Oauth2ProviderBuilder {
        val authentication = AuthenticationToken("tokens-principal")

        return Oauth2ProviderBuilder(authorizedClientServiceAndManager, authentication)
    }

    private data class AuthenticationToken(private val principal: String) : AbstractAuthenticationToken(emptyList()) {
        override fun getCredentials(): Any {
            return ""
        }

        override fun getPrincipal(): Any {
            return principal
        }
    }
}