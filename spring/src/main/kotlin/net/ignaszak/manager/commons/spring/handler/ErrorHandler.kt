package net.ignaszak.manager.commons.spring.handler

import net.ignaszak.manager.commons.error.Error.Default.UNKNOWN
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptor
import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.handler.mapper.ErrorHandlerMapper
import org.apache.logging.log4j.LogManager
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.Exception

@RestControllerAdvice
class ErrorHandler(private val errorDescriptor: ErrorDescriptor, private val errorHandlerMapper: ErrorHandlerMapper) {

    companion object {
        private val log = LogManager.getLogger(ErrorHandler::class.java)
    }

    @ExceptionHandler(ErrorException::class)
    fun handleErrorException(e: ErrorException): ResponseEntity<ErrorResponse> {
        log.error(e)
        val errorInfoResponseSet = errorHandlerMapper.errorTOSetToErrorInfoResponseSet(errorDescriptor.getErrors(e))
        return ResponseEntity
            .status(errorDescriptor.getHttpStatus(e))
            .body(ErrorResponse(errorInfoResponseSet))
    }

    @ExceptionHandler(Exception::class)
    fun handleException(e: Exception): ResponseEntity<ErrorResponse> {
        log.error(e)
        return ResponseEntity
            .badRequest()
            .body(ErrorResponse.of(UNKNOWN.code, UNKNOWN.message))
    }
}