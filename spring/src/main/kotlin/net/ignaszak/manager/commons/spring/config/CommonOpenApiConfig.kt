package net.ignaszak.manager.commons.spring.config

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.security.SecurityScheme
import io.swagger.v3.oas.models.servers.Server
import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.spring.application.ServiceApplicationInfo
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class CommonOpenApiConfig {

    @Bean
    open fun openApi(managerProperties: ManagerProperties, applicationContext: ApplicationContext): OpenAPI {
        val info = ServiceApplicationInfo(applicationContext)
        val gatewayExternalUri = managerProperties.server.gatewayExternalUri ?: ""
        val server = Server()
        server.url = "${gatewayExternalUri}/${info.getApiVersion()}/${info.getApplicationName()}"
        server.description = "Default server"

        return OpenAPI()
            .servers(listOf(server))
            .components(
                Components()
                    .addSecuritySchemes(
                        "User JWT",
                        SecurityScheme()
                            .type(SecurityScheme.Type.HTTP)
                            .description("User JWT")
                            .`in`(SecurityScheme.In.HEADER)
                            .scheme("Bearer")
                            .bearerFormat("JWT")
                            .name("Authorization")
                    )
            )
    }
}