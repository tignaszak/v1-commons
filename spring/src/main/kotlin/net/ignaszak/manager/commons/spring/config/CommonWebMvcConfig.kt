package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptor
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptorImpl
import net.ignaszak.manager.commons.httpclient.registry.ClientRegistrationFactory
import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.jwt.JwtServiceImpl
import net.ignaszak.manager.commons.restcriteria.filtering.FilterProcessorImpl
import net.ignaszak.manager.commons.restcriteria.paging.service.PagingService
import net.ignaszak.manager.commons.restcriteria.paging.service.PagingServiceImpl
import net.ignaszak.manager.commons.restcriteria.service.CriteriaService
import net.ignaszak.manager.commons.restcriteria.service.CriteriaServiceImpl
import net.ignaszak.manager.commons.spring.application.ServiceApplicationInfo
import net.ignaszak.manager.commons.spring.handler.MappingHandlerFacade
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcherImpl
import net.ignaszak.manager.commons.spring.security.resolver.ParameterFilterResolver
import net.ignaszak.manager.commons.spring.security.resolver.UserPrincipalResolver
import net.ignaszak.manager.commons.utils.datetime.DateTimeProviderImpl
import net.ignaszak.manager.commons.utils.datetime.IDateTimeProvider
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository
import org.springframework.web.client.RestTemplate
import org.springframework.web.filter.CharacterEncodingFilter
import org.springframework.web.method.HandlerTypePredicate
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
import java.nio.charset.StandardCharsets
import java.time.Duration
import java.time.LocalDateTime
import javax.servlet.Filter

@Suppress("SpringFacetCodeInspection")
@Configuration
@EnableConfigurationProperties(ManagerProperties::class)
@ComponentScan(
    "net.ignaszak.manager.commons.conf",
    "net.ignaszak.manager.commons.spring.handler",
    "net.ignaszak.manager.commons.spring.security",
    "net.ignaszak.manager.commons.spring.restcriteria"
)
open class CommonWebMvcConfig(private val managerProperties: ManagerProperties) : WebMvcConfigurer {

    companion object {
        private const val REST_TEMPLATE_TIMEOUT_SEC: Long = 3

        @JvmStatic
        fun handleRequestMappings(event: ContextRefreshedEvent, managerProperties: ManagerProperties) {
            val applicationContext = event.applicationContext
            val handlerMapping = applicationContext.getBean(RequestMappingHandlerMapping::class.java)
            val rabbitTemplate = applicationContext.getBean(RabbitTemplate::class.java)

            val applicationInfo = ServiceApplicationInfo(applicationContext)

            val facade = MappingHandlerFacade(handlerMapping, rabbitTemplate, managerProperties, applicationInfo)
            facade.process()
        }
    }

    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.add(UserPrincipalResolver())
        resolvers.add(ParameterFilterResolver(FilterProcessorImpl()))
        super.addArgumentResolvers(resolvers)
    }

    @Bean
    open fun clientRegistrationRepository(managerProperties: ManagerProperties): ClientRegistrationRepository {
        val factory = ClientRegistrationFactory(
            managerProperties.server.gatewayUri + managerProperties.oAuth2.tokenEndpoint,
            managerProperties.oAuth2.clients
        )

        return InMemoryClientRegistrationRepository(factory.getClientRegistrationList())
    }

    @Bean
    open fun characterUtf8EncodingFilter(): Filter {
        val filter = CharacterEncodingFilter()
        filter.encoding = StandardCharsets.UTF_8.displayName()
        filter.setForceEncoding(true)
        return filter
    }

    @Bean
    open fun errorDescriptor(): ErrorDescriptor {
        return ErrorDescriptorImpl()
    }

    @Bean
    open fun jwtService(): JwtService {
        return JwtServiceImpl(managerProperties.jwt.expirationMs, managerProperties.jwt.secret)
    }

    @Bean
    open fun uriMatcher(): UriMatcher {
        return UriMatcherImpl(RequestMappingRepositorySingleton)
    }

    @EventListener
    open fun handleContextRefresh(event: ContextRefreshedEvent) {
        handleRequestMappings(event, managerProperties)
    }

    @Bean
    open fun commonOpenApiConfigurer(@Value("\${springdoc.api-docs.path:/api-docs}") path: String) : CommonOpenApiConfigurer {
        return CommonOpenApiConfigurer(path)
    }

    @Bean
    open fun restTemplate(): RestTemplate {
        val timeout = Duration.ofSeconds(REST_TEMPLATE_TIMEOUT_SEC)

        val builder = RestTemplateBuilder()
        builder.setConnectTimeout(timeout)
        builder.setReadTimeout(timeout)

        return builder.build()
    }

    @Bean
    open fun pagingService(): PagingService {
        return PagingServiceImpl(managerProperties.server.pageSize)
    }

    @Bean
    open fun criteriaService(pagingService: PagingService): CriteriaService {
        return CriteriaServiceImpl(pagingService)
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    open fun dateTimeProvider() : IDateTimeProvider = DateTimeProviderImpl(LocalDateTime.now())

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.addPathPrefix(
            managerProperties.server.pathPrefix,
            HandlerTypePredicate.forBasePackage("net.ignaszak.manager")
        )
    }
}