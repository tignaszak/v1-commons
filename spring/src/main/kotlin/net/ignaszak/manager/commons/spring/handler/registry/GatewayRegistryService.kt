package net.ignaszak.manager.commons.spring.handler.registry

import net.ignaszak.manager.commons.conf.properties.JmsProperties.QueueProperties
import net.ignaszak.manager.commons.messaging.gateway.GatewayMappingMessage
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import org.springframework.amqp.rabbit.core.RabbitTemplate
import java.util.stream.Collectors

class GatewayRegistryService(private val rabbitTemplate: RabbitTemplate, private val gatewayProperties: QueueProperties) : RegistryServiceInterface {
    override fun register(mappings: Set<Mapping>) {
        val gatewayMappings = mappings.stream()
            .map {
                GatewayMappingMessage.Mapping(it.path, it.type.name)
            }
            .collect(Collectors.toSet())
        val gatewayMessage = GatewayMappingMessage(gatewayMappings)
        rabbitTemplate.convertAndSend(gatewayProperties.exchange, gatewayProperties.routingKey, gatewayMessage)
    }
}