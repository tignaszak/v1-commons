package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.utils.JsonUtils.toJson
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import java.nio.charset.StandardCharsets.UTF_8
import javax.servlet.http.HttpServletResponse

object WebConfigHelper {

    @JvmStatic
    fun setResponse(response: HttpServletResponse, httpStatus: HttpStatus, model: Any) {
        response.status = httpStatus.value()
        response.contentType = APPLICATION_JSON_VALUE
        response.characterEncoding = UTF_8.displayName()
        response.writer.use {
            it.write(
                toJson(model)
            )
        }
    }
}