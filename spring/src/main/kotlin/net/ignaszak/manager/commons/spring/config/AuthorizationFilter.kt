package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.error.AuthError
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptor
import net.ignaszak.manager.commons.error.descriptor.ErrorDescriptorImpl
import net.ignaszak.manager.commons.error.exception.ErrorException
import net.ignaszak.manager.commons.jwt.JwtConstants
import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.jwt.exception.JwtException
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.*
import net.ignaszak.manager.commons.spring.security.mapper.UserSecurityMapper
import net.ignaszak.manager.commons.spring.security.utils.SecurityUtils
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.*
import java.util.stream.Collectors
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthorizationFilter constructor(
        authenticationManager: AuthenticationManager,
        private val userSecurityMapper: UserSecurityMapper,
        private val jwtService: JwtService,
        private val jwtOAuth2Service: JwtOAuth2Service,
        private val uriMatcher: UriMatcher
) : BasicAuthenticationFilter(authenticationManager) {

    private companion object {
        val log: Logger = LogManager.getLogger(AuthorizationFilter::class.java)
    }

    private val errorDescriptor: ErrorDescriptor = ErrorDescriptorImpl()

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val mapping = uriMatcher.match(request.requestURI)

        when (mapping.type) {
            ALLOWED                -> chain.doFilter(request, response)
            INNER                  -> processInnerSecurity(mapping, chain, request, response)
            AUTHENTICATED,
            AUTHENTICATED_INACTIVE -> processJwtSecurity(chain, request, response)
        }
    }

    private fun processJwtSecurity(chain: FilterChain, request: HttpServletRequest, response: HttpServletResponse) {
        try {
            val tokenOptional = Optional.ofNullable(request.getHeader(JwtConstants.TOKEN_HEADER))
            if (tokenOptional.isPresent) {
                val (jwtUserTO, roles) = jwtService.getJwtTO(tokenOptional.get())
                val userPrincipalTO = userSecurityMapper.jwtUserTOToUserPrincipalTO(jwtUserTO)
                val authentication = SecurityUtils.getAuthentication(userPrincipalTO, roles)
                SecurityContextHolder.getContext().authentication = authentication

                chain.doFilter(request, response)
            } else {
                setErrorResponse(response)
            }
        } catch (e: JwtException) {
            log.error(e)
            setErrorResponse(response)
        }
    }

    private fun processInnerSecurity(
        mapping: Mapping,
        chain: FilterChain,
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        try {
            val scopes = Optional.ofNullable(request.getHeader(JwtConstants.TOKEN_HEADER))
                .map(jwtOAuth2Service::extractScopes)
                .orElse(emptySet())

            if (scopes.isNotEmpty() && scopes.containsAll(mapping.scope)) {
                chain.doFilter(request, response)
            } else {
                setErrorResponse(response)
            }
        } catch (e: ErrorException) {
            log.error(e)

            val errors = errorDescriptor.getErrors(e)
                .stream()
                .map {
                    ErrorInfoResponse(it.code, it.message)
                }
                .collect(Collectors.toSet())

            WebConfigHelper.setResponse(response, errorDescriptor.getHttpStatus(e), ErrorResponse(errors))
        }
    }

    private fun setErrorResponse(response: HttpServletResponse) {
        val errorResponse = ErrorResponse.of(AuthError.AUT_INVALID_TOKEN.code, AuthError.AUT_INVALID_TOKEN.message)
        WebConfigHelper.setResponse(response, HttpStatus.FORBIDDEN, errorResponse)
    }
}