package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.jwt.JwtOAuth2Service
import net.ignaszak.manager.commons.jwt.JwtService
import net.ignaszak.manager.commons.spring.security.mapper.UserSecurityMapper
import net.ignaszak.manager.commons.spring.security.matcher.UriMatcher
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy

@Configuration
@EnableWebSecurity
@Import(CommonWebMvcConfig::class)
open class CommonWebSecurityConfig(
    private val jwtService: JwtService,
    private val userSecurityMapper: UserSecurityMapper,
    private val uriMatcher: UriMatcher
) : WebSecurityConfigurerAdapter() {

    companion object {
        private val log: Logger = LogManager.getLogger()

        fun applyDefaultSecurity(http: HttpSecurity, filter: AuthorizationFilter) {
            http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(CommonForbiddenEntryPoint())
                .and()
                .addFilter(filter)
        }
    }

    override fun configure(http: HttpSecurity) {
        val filter = AuthorizationFilter(
            authenticationManager() ?: authenticationManagerBean(),
            userSecurityMapper,
            jwtService,
            getJwtOAuth2ServiceBean(),
            uriMatcher
        )

        applyDefaultSecurity(http, filter)
    }

    private fun getJwtOAuth2ServiceBean(): JwtOAuth2Service {
        return try {
            applicationContext.getBean(JwtOAuth2Service::class.java)
        } catch (e: NoSuchBeanDefinitionException) {
            log.warn("No bean 'JwtOAuth2Service' available")

            object : JwtOAuth2Service {}
        }
    }
}