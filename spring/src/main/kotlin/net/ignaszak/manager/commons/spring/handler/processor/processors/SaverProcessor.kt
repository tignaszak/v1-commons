package net.ignaszak.manager.commons.spring.handler.processor.processors

import net.ignaszak.manager.commons.spring.handler.processor.MappingProcessorInterface
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton

class SaverProcessor : MappingProcessorInterface {
    override fun process(mappings: Set<Mapping>) {
        mappings.forEach(RequestMappingRepositorySingleton::add)
    }
}