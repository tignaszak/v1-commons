package net.ignaszak.manager.commons.spring.restcriteria.mapper

import net.ignaszak.manager.commons.rest.filter.DateOperator
import net.ignaszak.manager.commons.rest.filter.NumberOperator
import net.ignaszak.manager.commons.rest.filter.Operator
import net.ignaszak.manager.commons.rest.filter.StringOperator
import net.ignaszak.manager.commons.rest.page.PageRequest
import net.ignaszak.manager.commons.rest.page.PageResponse
import net.ignaszak.manager.commons.restcriteria.filtering.to.*
import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO
import net.ignaszak.manager.commons.restcriteria.paging.to.PageTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.stereotype.Component

@Component
@Mapper(componentModel = "spring")
interface RestCriteriaMapper {
    @Mapping(source = "page", target = "page", defaultValue = "1")
    fun pageRequestToPageCriteriaTO(pageRequest: PageRequest): PageCriteriaTO
    fun pageTOToPageResponse(pageTO: PageTO): PageResponse
    fun <T, O> anyOperatorToOperatorTO(operator: O?) : OperatorTO<T>? where O: Operator<T> {
        return operator?.type?.name?.let {
            OperatorTO(OperatorTO.Type.valueOf(it), operator.value)
        }
    }

    fun stringOperatorToStringOperatorTO(operator: StringOperator?): StringOperatorTO? {
        return operator?.type?.name?.let {
            StringOperatorTO(OperatorTO.Type.valueOf(it), operator.value)
        }
    }

    fun <E: Enum<E>> stringOperatorToEnumOperatorTO(operator: StringOperator?, enum: Class<E>): EnumOperatorTO<E>? {
        return operator?.type?.name?.let {
            val enums = mutableSetOf<E>()
            operator.value.forEach {value ->
                enum.enumConstants.firstOrNull {enumClass ->
                    enumClass.name == value
                }?.let(enums::add)
            }

            EnumOperatorTO(OperatorTO.Type.valueOf(it), enums)
        }
    }

    fun numberOperatorToNumberOperatorTO(operator: NumberOperator?): NumberOperatorTO? {
        return operator?.first?.type?.name?.let {
            NumberOperatorTO(
                OperatorTO(OperatorTO.Type.valueOf(it), operator.first?.value!!),
                if (operator.second != null) OperatorTO(OperatorTO.Type.valueOf(operator.second?.type?.name!!), operator.second?.value!!) else null
            )
        }
    }

    fun dateOperatorToDateOperatorTO(operator: DateOperator?): DateOperatorTO? {
        return operator?.first?.type?.name?.let {
            DateOperatorTO(
                OperatorTO(OperatorTO.Type.valueOf(it), operator.first?.value!!),
                if (operator.second != null) OperatorTO(OperatorTO.Type.valueOf(operator.second?.type?.name!!), operator.second?.value!!) else null
            )
        }
    }
}