package net.ignaszak.manager.commons.spring.security.resolver

import net.ignaszak.manager.commons.restcriteria.filtering.FilterProcessorException
import net.ignaszak.manager.commons.restcriteria.filtering.IFilterProcessor
import net.ignaszak.manager.commons.restcriteria.filtering.annotation.ParameterFilter
import net.ignaszak.manager.commons.utils.JsonUtils
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.core.MethodParameter
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer
import java.net.URLDecoder
import java.nio.charset.StandardCharsets.UTF_8

class ParameterFilterResolver(private val filterProcessor: IFilterProcessor) : HandlerMethodArgumentResolver {

    companion object {
        private val logger: Logger = LogManager.getLogger()
    }

    override fun supportsParameter(parameter: MethodParameter) =
        parameter.getParameterAnnotation(ParameterFilter::class.java) != null

    override fun resolveArgument(
        parameter: MethodParameter,
        mavContainer: ModelAndViewContainer?,
        webRequest: NativeWebRequest,
        binderFactory: WebDataBinderFactory?
    ): Any {
        val type = parameter.parameterType

        try {
            parameter.parameterName?.let { queryName ->
                webRequest.getParameter(queryName)?.let {
                    val decoded = URLDecoder.decode(it, UTF_8)
                    return filterProcessor.process(decoded, type)
                }
            }
        } catch (e: FilterProcessorException) {
            logger.info(e)
        } catch (e: IllegalArgumentException) {
            logger.error(e)
        }

        return JsonUtils.toObject("{}", type)
    }
}