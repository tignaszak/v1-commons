package net.ignaszak.manager.commons.spring.security.utils

import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import java.util.stream.Collectors

object SecurityUtils {

    @JvmStatic
    fun getAuthentication(userPrincipalTO: UserPrincipalTO, roles: Set<String>): UsernamePasswordAuthenticationToken {
        val authorities = roles.stream()
            .map {role: String -> SimpleGrantedAuthority(role)}
            .collect(Collectors.toSet())
        return UsernamePasswordAuthenticationToken(userPrincipalTO, null, authorities)
    }
}