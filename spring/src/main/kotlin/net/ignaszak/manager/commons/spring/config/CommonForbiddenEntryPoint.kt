package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.error.AuthError.AUT_AUTHENTICATION
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.spring.config.WebConfigHelper.setResponse
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CommonForbiddenEntryPoint : AuthenticationEntryPoint {
    override fun commence(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authException: AuthenticationException
    ) {
        setResponse(
            response,
            FORBIDDEN,
            ErrorResponse.of(AUT_AUTHENTICATION.code, AUT_AUTHENTICATION.message)
        )
    }
}