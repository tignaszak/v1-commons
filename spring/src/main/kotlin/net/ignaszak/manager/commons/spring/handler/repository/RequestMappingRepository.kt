package net.ignaszak.manager.commons.spring.handler.repository

interface RequestMappingRepository {
    fun add(mapping: Mapping)
    fun findAll(): Set<Mapping>
    fun clear()
}