package net.ignaszak.manager.commons.spring.handler.processor.processors

import net.ignaszak.manager.commons.spring.application.ApplicationInfoInterface
import net.ignaszak.manager.commons.spring.handler.processor.MappingProcessorInterface
import net.ignaszak.manager.commons.spring.handler.registry.RegistryServiceInterface
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.*
import java.util.stream.Collectors

class MappingRegistryProcessor(private val registryService: RegistryServiceInterface,
                               private val applicationInfo: ApplicationInfoInterface) : MappingProcessorInterface {
    override fun process(mappings: Set<Mapping>) {
        val mappingsToRegister = mappings.stream()
            .map(this::innerAsAllowed)
            .filter(this::shouldRegister)
            .map(this::addAppInfo)
            .collect(Collectors.toSet())

        if (mappingsToRegister.isNotEmpty()) {
            registryService.register(mappingsToRegister)
        }
    }

    private fun innerAsAllowed(mapping: Mapping): Mapping {
        return if (mapping.type == INNER) Mapping(mapping.path, ALLOWED) else mapping
    }

    private fun shouldRegister(mapping: Mapping): Boolean {
        return mapping.type == ALLOWED || mapping.type == AUTHENTICATED_INACTIVE
    }

    private fun addAppInfo(mapping: Mapping): Mapping {
        return Mapping(
            "/${applicationInfo.getApiVersion()}/${applicationInfo.getApplicationName()}${mapping.path}",
            mapping.type
        )
    }
}