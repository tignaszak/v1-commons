package net.ignaszak.manager.commons.spring.handler.mapper

import net.ignaszak.manager.commons.error.to.ErrorTO
import net.ignaszak.manager.commons.rest.response.ErrorInfoResponse
import org.mapstruct.Mapper
import org.springframework.stereotype.Component

@Component
@Mapper(componentModel = "spring")
interface ErrorHandlerMapper {
    fun errorTOSetToErrorInfoResponseSet(errorTOSet: Set<ErrorTO>): Set<ErrorInfoResponse>
}