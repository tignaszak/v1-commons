package net.ignaszak.manager.commons.spring.application

interface ApplicationInfoInterface {
    fun getApplicationName(): String
    fun getApiVersion(): String
}