package net.ignaszak.manager.commons.spring.security.resolver

import net.ignaszak.manager.commons.user.annotation.UserPrincipal
import net.ignaszak.manager.commons.user.to.UserPrincipalTO
import org.springframework.core.MethodParameter
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer

class UserPrincipalResolver : HandlerMethodArgumentResolver {
    override fun supportsParameter(parameter: MethodParameter): Boolean {
        return parameter.getParameterAnnotation(UserPrincipal::class.java) != null
    }

    override fun resolveArgument(
        parameter: MethodParameter,
        mavContainer: ModelAndViewContainer?,
        webRequest: NativeWebRequest,
        binderFactory: WebDataBinderFactory?
    ): UserPrincipalTO {
        return SecurityContextHolder.getContext().authentication.principal as UserPrincipalTO
    }
}