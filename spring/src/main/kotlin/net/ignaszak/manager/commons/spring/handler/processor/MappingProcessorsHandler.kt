package net.ignaszak.manager.commons.spring.handler.processor

import net.ignaszak.manager.commons.spring.handler.repository.Mapping

class MappingProcessorsHandler {
    private val processors: MutableSet<MappingProcessorInterface> = LinkedHashSet()

    fun add(processor: MappingProcessorInterface) {
        processors.add(processor)
    }

    fun handle(mappings: Set<Mapping>) {
        processors.forEach {
            it.process(mappings)
        }
    }
}