package net.ignaszak.manager.commons.spring.handler.collector

import net.ignaszak.manager.commons.conf.properties.ServerProperties
import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import java.util.Collections.emptySet
import java.util.stream.Collectors

class PropertiesRequestMappingCollector(private val serverProperties: ServerProperties) : RequestMappingCollector {
    override fun collect(): Set<Mapping> {
        return serverProperties.mappings
            ?.stream()
            ?.map {
                if (it.scope != null) {
                    Mapping(it.path, Mapping.Type.valueOf(it.type.uppercase()), it.scope!!)
                } else {
                    Mapping(it.path, Mapping.Type.valueOf(it.type.uppercase()))
                }
            }
            ?.collect(Collectors.toSet())
            ?: emptySet()
    }
}