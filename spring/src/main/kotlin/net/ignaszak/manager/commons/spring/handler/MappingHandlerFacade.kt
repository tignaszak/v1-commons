package net.ignaszak.manager.commons.spring.handler

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.spring.application.ApplicationInfoInterface
import net.ignaszak.manager.commons.spring.handler.collector.AnnotationRequestMappingCollector
import net.ignaszak.manager.commons.spring.handler.collector.PropertiesRequestMappingCollector
import net.ignaszak.manager.commons.spring.handler.processor.MappingProcessorsHandler
import net.ignaszak.manager.commons.spring.handler.processor.processors.MappingRegistryProcessor
import net.ignaszak.manager.commons.spring.handler.processor.processors.SaverProcessor
import net.ignaszak.manager.commons.spring.handler.registry.GatewayRegistryService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

class MappingHandlerFacade(private val requestMappingHandler: RequestMappingHandlerMapping,
                           private val rabbitTemplate: RabbitTemplate,
                           private val managerProperties: ManagerProperties,
                           private val applicationInfo: ApplicationInfoInterface) {

    private companion object {
        val log: Logger = LogManager.getLogger(MappingHandlerFacade::class.java)
    }

    fun process() {
        val processorsHandler = MappingProcessorsHandler()

        managerProperties.jms.queues["gateway"]?.let {
            val gatewayRegistryService = GatewayRegistryService(rabbitTemplate, it)
            processorsHandler.add(MappingRegistryProcessor(gatewayRegistryService, applicationInfo))
        } ?: log.error("Queue with id 'gateway' is not defined!")

        processorsHandler.add(SaverProcessor())

        val annotationCollector = AnnotationRequestMappingCollector(requestMappingHandler, managerProperties.server.pathPrefix)
        val propertiesCollector = PropertiesRequestMappingCollector(managerProperties.server)
        val mappings = annotationCollector.collect().union(propertiesCollector.collect())

        processorsHandler.handle(mappings)
    }
}