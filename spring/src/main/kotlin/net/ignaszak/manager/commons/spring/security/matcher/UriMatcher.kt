package net.ignaszak.manager.commons.spring.security.matcher

import net.ignaszak.manager.commons.spring.handler.repository.Mapping

interface UriMatcher {
    fun match(uri: String): Mapping
}