package net.ignaszak.manager.commons.spring.handler.collector

import net.ignaszak.manager.commons.spring.handler.repository.Mapping

interface RequestMappingCollector {
    fun collect() : Set<Mapping>
}