package net.ignaszak.manager.commons.spring.handler.repository

import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED
import net.ignaszak.manager.commons.spring.handler.repository.Mapping.Type.AUTHENTICATED_INACTIVE

data class Mapping(val path: String, val type: Type, val scope: Set<String>) {
    constructor(path: String, type: Type): this(path, type, setOf())

    fun isAuthenticated() = type == AUTHENTICATED || type == AUTHENTICATED_INACTIVE

    enum class Type {
        ALLOWED, INNER, AUTHENTICATED, AUTHENTICATED_INACTIVE
    }
}
