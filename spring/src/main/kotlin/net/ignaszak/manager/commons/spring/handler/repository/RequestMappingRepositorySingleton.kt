package net.ignaszak.manager.commons.spring.handler.repository

import org.springframework.stereotype.Component

@Component
object RequestMappingRepositorySingleton : RequestMappingRepository {

    private val mappings = mutableMapOf<String, Mapping>()

    override fun add(mapping: Mapping) {
        mappings[mapping.path] = mapping
    }

    override fun findAll(): Set<Mapping> {
        return mappings.values.toSet()
    }

    override fun clear() {
        mappings.clear()
    }
}