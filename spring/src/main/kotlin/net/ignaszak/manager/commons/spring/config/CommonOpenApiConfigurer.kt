package net.ignaszak.manager.commons.spring.config

import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepositorySingleton
import org.apache.logging.log4j.LogManager

class CommonOpenApiConfigurer(path: String) {
    companion object {
        private val log = LogManager.getLogger(CommonOpenApiConfigurer::class.java)
    }

    init {
        try {
            if (path.isNotBlank()) {
                Class.forName("io.swagger.v3.oas.models.OpenAPI")

                RequestMappingRepositorySingleton.add(Mapping(path, Mapping.Type.ALLOWED))
            }
        } catch (e: ClassNotFoundException) {
            log.info("Springdoc openapi is not configured!", e)
        }
    }
}