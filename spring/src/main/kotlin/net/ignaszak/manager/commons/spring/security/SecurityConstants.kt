package net.ignaszak.manager.commons.spring.security

object SecurityConstants {
    const val AUTH_LOGIN_URL = "/auth/login"
    const val AUTH_REGISTRATION_URL = "/auth/registration"

    const val AUTH_PARAM_USERNAME = "username"
    const val AUTH_PARAM_PASSWORD = "password"
}