package net.ignaszak.manager.commons.spring.handler.processor

import net.ignaszak.manager.commons.spring.handler.repository.Mapping

interface MappingProcessorInterface {
    fun process(mappings: Set<Mapping>)
}