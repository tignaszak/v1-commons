package net.ignaszak.manager.commons.spring.handler.annotation

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class AllowedMapping
