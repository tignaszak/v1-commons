package net.ignaszak.manager.commons.spring.application

import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
class ServiceApplicationInfo(private val context: ApplicationContext) : ApplicationInfoInterface {

    private val applicationName: String
    private val apiVersion: String

    init {
        val matchResult = context.id?.let {
            "manager-(?<name>[a-zA-Z]+)-(?<version>v[0-9\\.]+)-service"
                .toRegex()
                .find(it)
        }

        if (matchResult != null) {
            applicationName = matchResult.groups["name"]?.value ?: ""
            apiVersion = matchResult.groups["version"]?.value ?: ""
        } else {
            applicationName = ""
            apiVersion = ""
        }
    }

    override fun getApplicationName(): String {
        return applicationName
    }

    override fun getApiVersion(): String {
        return apiVersion
    }
}