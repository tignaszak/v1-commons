package net.ignaszak.manager.commons.spring.security.matcher

import net.ignaszak.manager.commons.spring.handler.repository.Mapping
import net.ignaszak.manager.commons.spring.handler.repository.RequestMappingRepository
import org.apache.logging.log4j.LogManager
import java.util.regex.PatternSyntaxException

class UriMatcherImpl(private val repository: RequestMappingRepository, private val prefix: String) : UriMatcher {

    companion object {
        private val log = LogManager.getLogger(UriMatcherImpl::class.java)
    }

    constructor(repository: RequestMappingRepository): this(repository, "")

    override fun match(uri: String): Mapping = repository.findAll()
        .stream()
        .filter {
            match("$prefix${it.path}", uri)
        }
        .findFirst()
        .orElseGet {
            Mapping(uri, Mapping.Type.AUTHENTICATED)
        }

    private fun match(path: String, uri: String) = try {
        uri.matches(toRegex(path))
    } catch (e: PatternSyntaxException) {
        log.error("Could not parse string to regex!", e)

        false
    }

    private fun toRegex(path: String) = path
        .replace("**", ".*")
        .replace("/", "\\/")
        .replace("\\{([a-zA-Z\\d_-]+)}".toRegex(), "[^\\/]*")
        .toRegex()
}