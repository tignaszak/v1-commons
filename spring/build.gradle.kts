plugins {
    `maven-publish`
    kotlin("kapt")
}

dependencies {
    implementation(project(":conf"))
    implementation(project(":error"))
    implementation(project(":http-client"))
    implementation(project(":jwt"))
    implementation(project(":messaging"))
    implementation(project(":rest"))
    implementation(project(":rest-criteria"))
    implementation(project(":user"))
    implementation(project(":utils"))

    implementation(Conf.Deps.APACHE_HTTPCLIENT)
    implementation(Conf.Deps.MAPSTRUCT)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_AMQP)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_OAUTH2_CLIENT)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_SECURITY)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_WEB)
    implementation(Conf.Deps.SWAGGER_MODELS)

    testImplementation(project(":test"))

    testImplementation(Conf.Deps.GSON)
    testImplementation(Conf.Deps.OPENAPI)
    testImplementation(Conf.Deps.SPOCK_SPRING)
    testImplementation(Conf.Deps.SPRING_BOOT_STARTER_TEST)
    testImplementation(Conf.Deps.SPRING_FRAMEWORK_TEST)
    testImplementation(Conf.Deps.SPRING_SECURITY_TEST)

    kapt(Conf.Deps.MAPSTRUCT_PROCESSOR)
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.SPRING
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + Conf.Modules.SPRING.replace("-", "/")) {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}