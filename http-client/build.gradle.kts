plugins {
    `maven-publish`
}

dependencies {
    implementation(project(":conf"))
    implementation(project(":error"))
    implementation(project(":rest"))
    implementation(project(":utils"))

    implementation(Conf.Deps.GSON)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_OAUTH2_CLIENT)

    testImplementation(project(":test"))
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.HTTP_CLIENT
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + "manager/commons/http-client") {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}