package net.ignaszak.manager.commons.httpclient.client.provider

import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import java.lang.IllegalArgumentException
import java.util.*
import java.util.function.Supplier

class Oauth2ProviderBuilder(
    private val authorizedClientService: AuthorizedClientServiceOAuth2AuthorizedClientManager,
    private val principal: Authentication
) {
    private var clientRegistrationId: String = ""

    fun withClientRegistrationId(value: String) = apply { clientRegistrationId = value }

    fun build() : Supplier<Optional<OAuth2AuthorizedClient>> {
        if (clientRegistrationId.isBlank()) {
            throw IllegalArgumentException("Client registration id could not be blank!")
        }

        return OAuth2Provider(authorizedClientService, principal, clientRegistrationId)
    }
}