package net.ignaszak.manager.commons.httpclient

import org.springframework.http.HttpMethod
import org.springframework.http.HttpMethod.POST

data class Request(val url: String, val method: HttpMethod, val body: Any) {
    companion object {
        @JvmStatic
        fun withUrl(url: String) = Builder(url)
    }

    class Builder(private val url: String) {
        var method: HttpMethod = POST
        var body: Any = object {}

        fun method (value: HttpMethod) = apply { method = value }
        fun body (value: Any) = apply { body = value }

        fun build() = Request(url, method, body)
    }
}
