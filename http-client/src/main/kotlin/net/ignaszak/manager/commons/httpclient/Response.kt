package net.ignaszak.manager.commons.httpclient

import org.springframework.http.HttpStatus
import java.util.*

data class Response(val body: Optional<Any>, val status: HttpStatus)
