package net.ignaszak.manager.commons.httpclient.client

import com.google.gson.JsonSyntaxException
import net.ignaszak.manager.commons.error.Error.Default.UNKNOWN
import net.ignaszak.manager.commons.httpclient.Request
import net.ignaszak.manager.commons.httpclient.Response
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import net.ignaszak.manager.commons.utils.JsonUtils.toObject
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.core.OAuth2AuthorizationException
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import java.util.*
import java.util.function.Supplier

class OAuth2HttpClient (
    private val restTemplate: RestTemplate,
    private val oAuth2Provider: Supplier<Optional<OAuth2AuthorizedClient>>
) : HttpClient {

    @Throws(HttpClientException::class)
    override fun send(request: Request): Response {
        try {
            return oAuth2Provider.get()
                .map {
                    doRequest(it.accessToken.tokenValue, request)
                }
                .orElseThrow {
                    HttpClientException("Invalid oAuth2 token")
                }
        } catch (e: OAuth2AuthorizationException) {
            throw HttpClientException("Error while sending request", e)
        } catch (e: HttpClientErrorException) {
            return Response(
                parseBody(e.responseBodyAsString),
                e.statusCode
            )
        }
    }

    private fun doRequest(accessToken: String, request: Request): Response {
        val headers = HttpHeaders()
        headers.add("Authorization", "Bearer $accessToken")
        val httpEntity = HttpEntity<Any>(request.body, headers)

        val responseEntity = restTemplate.exchange(request.url, request.method, httpEntity, Any::class.java)

        return Response(
            Optional.ofNullable(responseEntity.body),
            responseEntity.statusCode
        )
    }

    private fun parseBody(body: String?): Optional<Any> {
        return if (body != null) {
            val response = try {
                toObject(body, ErrorResponse::class.java)
            } catch (e: JsonSyntaxException) {
                ErrorResponse.of(UNKNOWN.code, body)
            }

            Optional.ofNullable(response)
        } else {
            Optional.empty()
        }
    }
}