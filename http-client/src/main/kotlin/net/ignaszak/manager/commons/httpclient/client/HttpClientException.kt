package net.ignaszak.manager.commons.httpclient.client

import java.lang.Exception

class HttpClientException : Exception {
    constructor(message: String): super(message)
    constructor(message: String, cause: Throwable): super(message, cause)
}