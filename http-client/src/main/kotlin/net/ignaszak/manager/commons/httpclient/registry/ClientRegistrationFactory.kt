package net.ignaszak.manager.commons.httpclient.registry

import net.ignaszak.manager.commons.conf.properties.OAuth2Properties.CredentialProperties
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.core.AuthorizationGrantType.CLIENT_CREDENTIALS
import java.util.stream.Collectors

class ClientRegistrationFactory(private val tokenUri: String, private val clients: Map<String, CredentialProperties>) {

    fun getClientRegistrationList(): List<ClientRegistration> {
        return clients.entries
            .stream()
            .map {
                ClientRegistration
                    .withRegistrationId(it.key)
                    .tokenUri(tokenUri)
                    .clientId(it.value.id)
                    .clientSecret(it.value.secret)
                    .scope(it.value.scope)
                    .authorizationGrantType(CLIENT_CREDENTIALS)
                    .build()
            }
            .collect(Collectors.toList())
    }
}