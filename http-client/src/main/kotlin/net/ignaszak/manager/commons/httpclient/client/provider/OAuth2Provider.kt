package net.ignaszak.manager.commons.httpclient.client.provider

import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import java.util.*
import java.util.function.Supplier

class OAuth2Provider(
    private val authorizedClientService: AuthorizedClientServiceOAuth2AuthorizedClientManager,
    private val principal: Authentication,
    private val clientRegistrationId: String
) : Supplier<Optional<OAuth2AuthorizedClient>> {
    override fun get(): Optional<OAuth2AuthorizedClient> {
        val authorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId(clientRegistrationId)
            .principal(principal)
            .build()

        return Optional.ofNullable(authorizedClientService.authorize(authorizeRequest))
    }
}