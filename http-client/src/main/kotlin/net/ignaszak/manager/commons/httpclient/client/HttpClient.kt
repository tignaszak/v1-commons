package net.ignaszak.manager.commons.httpclient.client

import net.ignaszak.manager.commons.httpclient.Request
import net.ignaszak.manager.commons.httpclient.Response

interface HttpClient {
    fun send(request: Request): Response
}