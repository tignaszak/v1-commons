package net.ignaszak.manager.commons.httpclient.registry

import net.ignaszak.manager.commons.conf.properties.OAuth2Properties
import org.springframework.security.oauth2.client.registration.ClientRegistration
import spock.lang.Specification

class ClientRegistrationFactorySpec extends Specification {
    private static final String ANY_TOKEN_URI = "any token uri"
    private static final Set<String> CLIENT_ONE_SCOPE = Set.of("client-one-scope-1", "client-one-scope-2")
    private static final Set<String> CLIENT_TWO_SCOPE = Set.of("client-two-scope-1")
    private static final String CLIENT_ONE_ID = "client-id-one"
    private static final String CLIENT_ONE_SECRET = "client-secret-one"
    private static final String CLIENT_TWO_ID = "client-id-two"
    private static final String CLIENT_TWO_SECRET = "client-secret-two"
    private static final String CLIENT_ONE_REGISTRATION_ID = "client1"
    private static final String CLIENT_TWO_REGISTRATION_ID = "client2"

    private static final Map<String, OAuth2Properties.CredentialProperties> CLIENTS = Map.of(
            CLIENT_ONE_REGISTRATION_ID,
            new OAuth2Properties.CredentialProperties(CLIENT_ONE_ID, CLIENT_ONE_SECRET, CLIENT_ONE_SCOPE),
            CLIENT_TWO_REGISTRATION_ID,
            new OAuth2Properties.CredentialProperties(CLIENT_TWO_ID, CLIENT_TWO_SECRET, CLIENT_TWO_SCOPE)
    )

    private subject = new ClientRegistrationFactory(ANY_TOKEN_URI, CLIENTS)

    def "test client registration" () {
        when:
        List<ClientRegistration> result = subject.getClientRegistrationList()

        then:
        result.size() == 2
    }
}
