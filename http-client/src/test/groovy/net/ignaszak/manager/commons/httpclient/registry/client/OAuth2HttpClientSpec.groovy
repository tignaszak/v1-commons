package net.ignaszak.manager.commons.httpclient.registry.client

import net.ignaszak.manager.commons.httpclient.Request
import net.ignaszak.manager.commons.httpclient.client.HttpClientException
import net.ignaszak.manager.commons.httpclient.client.OAuth2HttpClient
import net.ignaszak.manager.commons.rest.response.ErrorResponse
import org.springframework.http.ResponseEntity
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.core.OAuth2AccessToken
import org.springframework.security.oauth2.core.OAuth2AuthorizationException
import org.springframework.security.oauth2.core.OAuth2Error
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import java.util.function.Supplier

import static net.ignaszak.manager.commons.error.Error.Default.UNKNOWN
import static net.ignaszak.manager.commons.utils.JsonUtils.toJson
import static org.springframework.http.HttpMethod.POST
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.OK
import static org.springframework.security.oauth2.core.AuthorizationGrantType.CLIENT_CREDENTIALS
import static org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType.BEARER

class OAuth2HttpClientSpec extends Specification {

    private static final String ANY_REQUEST_URL = "request url"
    private static final String ANY_REQUEST_BODY = "request body"
    private static final String ANY_RESPONSE_BODY = "response body"
    private static final String ANY_JWT_TOKEN = "any jwt token"
    public static final String ANY_UNKNOWN_ERROR = "any unknown error"

    private RestTemplate restTemplateMock = Mock()

    private oAuth2Provider = new Supplier() {
        @Override
        Optional<OAuth2AuthorizedClient> get() {
            return Optional.of(
                    new OAuth2AuthorizedClient(
                            anyClientRegistration(),
                            "name",
                            new OAuth2AccessToken(BEARER, ANY_JWT_TOKEN, null, null)
                    )
            )
        }
    }

    private emptyOAuth2Provider = new Supplier() {
        @Override
        Optional<OAuth2AuthorizedClient> get() {
            return Optional.empty()
        }
    }

    private exceptionOnOAuth2Provider = new Supplier() {
        @Override
        Optional<OAuth2AuthorizedClient> get() {
            throw new OAuth2AuthorizationException(new OAuth2Error("error code"))
        }
    }

    private requestedServiceResponseErrorOAuth2Provider = new Supplier() {
        @Override
        Optional<OAuth2AuthorizedClient> get() {
            throw new HttpClientErrorException(BAD_REQUEST, "", null, toJson(anyErrorResponse()).bytes, null)
        }
    }

    private requestedServiceUnknownErrorOAuth2Provider = new Supplier() {
        @Override
        Optional<OAuth2AuthorizedClient> get() {
            throw new HttpClientErrorException(BAD_REQUEST, "", null, ANY_UNKNOWN_ERROR.bytes, null)
        }
    }

    def "get response with given request and oAuth2 provider" () {
        given:
        def request = anyRequest()
        1 * restTemplateMock.exchange(ANY_REQUEST_URL, POST, _, _) >> new ResponseEntity<>(ANY_RESPONSE_BODY, OK)
        def subject = new OAuth2HttpClient(restTemplateMock, oAuth2Provider)

        when:
        def response = subject.send(request)

        then:
        response.status == OK
        response.body.isPresent()
        response.body.get() == ANY_RESPONSE_BODY
    }

    def "throws exception on empty oAuth2 token"() {
        given:
        def request = anyRequest()
        def subject = new OAuth2HttpClient(restTemplateMock, emptyOAuth2Provider)

        when:
        subject.send(request)

        then:
        def e = thrown(HttpClientException)
        e.getMessage() == "Invalid oAuth2 token"
    }

    def "throws exception on any error"() {
        given:
        def request = anyRequest()
        def subject = new OAuth2HttpClient(restTemplateMock, exceptionOnOAuth2Provider)

        when:
        subject.send(request)

        then:
        def e = thrown(HttpClientException)
        e.getMessage() == "Error while sending request"
    }

    def "returns error response if requested service produces an ResponseError" () {
        given:
        def request = anyRequest()
        def subject = new OAuth2HttpClient(restTemplateMock, requestedServiceResponseErrorOAuth2Provider)

        when:
        def response = subject.send(request)

        then:
        response.status == BAD_REQUEST
        response.body.isPresent()
        response.body.get() == anyErrorResponse()
    }

    def "returns error response if requested service produces an unknown error" () {
        given:
        def request = anyRequest()
        def subject = new OAuth2HttpClient(restTemplateMock, requestedServiceUnknownErrorOAuth2Provider)

        when:
        def response = subject.send(request)

        then:
        response.status == BAD_REQUEST
        response.body.isPresent()
        response.body.get() == anyUnknownError()
    }

    private static Request anyRequest() {
        Request.withUrl(ANY_REQUEST_URL)
                .method(POST)
                .body(ANY_REQUEST_BODY)
                .build()
    }

    private static ClientRegistration anyClientRegistration() {
        ClientRegistration.withRegistrationId("id")
                .authorizationGrantType(CLIENT_CREDENTIALS)
                .clientId("clientId")
                .clientSecret("clientSecret")
                .tokenUri("token uri")
                .build()
    }

    private static ErrorResponse anyErrorResponse() {
        ErrorResponse.of(UNKNOWN.code, UNKNOWN.message)
    }

    private static ErrorResponse anyUnknownError() {
        ErrorResponse.of(UNKNOWN.code, ANY_UNKNOWN_ERROR)
    }
}
