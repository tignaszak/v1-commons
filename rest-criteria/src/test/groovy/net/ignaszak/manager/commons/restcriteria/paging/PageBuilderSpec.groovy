package net.ignaszak.manager.commons.restcriteria.paging

import spock.lang.Specification

class PageBuilderSpec extends Specification {

    def "test page builder" () {
        when:
        def pageTo = new PageBuilder()
            .pageNumber(2)
            .pageSize(20)
            .totalElements(356)
            .build()

        then:
        pageTo.number == 2
        pageTo.size == 20
        pageTo.totalPages == 18
        pageTo.totalElements == 356
    }

    def "test throw exception with insufficient data" () {
        when:
        new PageBuilder().build()

        then:
        def e = thrown PagingException
        e.message == "Incorrect data!"
    }
}
