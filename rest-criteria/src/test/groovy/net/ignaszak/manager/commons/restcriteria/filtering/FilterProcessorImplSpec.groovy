package net.ignaszak.manager.commons.restcriteria.filtering

import net.ignaszak.manager.commons.rest.filter.DateOperator
import net.ignaszak.manager.commons.rest.filter.NumberOperator
import net.ignaszak.manager.commons.rest.filter.Operator
import net.ignaszak.manager.commons.rest.filter.StringOperator
import spock.lang.Specification

import java.time.LocalDateTime

class FilterProcessorImplSpec extends Specification {
    private IFilterProcessor subject = new FilterProcessorImpl()

    private static class Model {
        DateOperator dateSet
        StringOperator chars
        NumberOperator operator
        Set<String> param
        List<String> list
        String single
        String singleAgain
        LocalDateTime date
        List<String> array
        NumberOperator complexOperator
        DateOperator date2
    }

    def "throw exception with invalid filter value" () {
        when:
        subject.process("", Model.class)

        then:
        thrown FilterProcessorException
    }

    def "process filter"() {
        given:
        def now = LocalDateTime.now()
        def filter =
                "dateSet:[2022-01-13,2022-01-14]|" +
                "param:[value,,1,value2,value||3]|" +
                " list: [a, b , c] | " +
                "single:[singleValue]|" +
                "singleAgain::first,second|||" +
                "array:single|" +
                "operator>1|" +
                "complexOperator>10<15|" +
                "date:${now}|" +
                "date2<:${now}>:2022-01-13T17:09:42"

        when:
        Model model = subject.process(filter, Model.class)

        then:
        model.dateSet != null
        model.dateSet.second == null
        model.dateSet.first.type == Operator.Type.EQUALS
        model.dateSet.first.value.size() == 2

        model.operator != null
        model.operator.first.value[0] == 1
        model.operator.first.type == Operator.Type.GREATER
        model.operator.second == null

        model.param != null
        model.param.size() == 3
        model.param.containsAll(Set.of("value,1", "value2", "value|3"))

        model.list != null
        model.list.size() == 3
        model.list.containsAll(Set.of("a", " b ", " c"))

        model.single == "singleValue"

        model.singleAgain == "first,second|"

        model.date == now

        model.array != null
        model.array.size() == 1
        model.array.containsAll(Set.of("single"))

        model.complexOperator != null
        model.complexOperator.first.type == Operator.Type.GREATER
        model.complexOperator.first.value[0] == 10
        model.complexOperator.second.type == Operator.Type.LOWER
        model.complexOperator.second.value[0] == 15

        model.date2 != null
        model.date2.first.type == Operator.Type.LOWER_OR_EQUALS
        model.date2.second.type == Operator.Type.GREATER_OR_EQUALS
    }

    def "test special chars"(String filter, String type, Set<String> values) {
        when:
        def model = subject.process(filter, Model.class)

        then:
        model.chars.type.character == type
        model.chars.value.containsAll(values)

        where:
        filter                            | type | values
        "chars:bla:!:{} &amp;%7C"         | ":"  | Set.of("bla:!:{} &amp;%7C")
        "chars:bla1,,bla2||param:aa,bb"   | ":"  | Set.of("bla1,,bla2|param:aa,bb")
        "chars:[bla1,,bla2||param:aa,bb]" | ":"  | Set.of("bla1,bla2|param:aa", "bb")
    }
}
