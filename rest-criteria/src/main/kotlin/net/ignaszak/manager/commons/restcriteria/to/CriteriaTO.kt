package net.ignaszak.manager.commons.restcriteria.to

import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO

data class CriteriaTO<F>(val pageCriteriaTO: PageCriteriaTO, val filterTO: F)
