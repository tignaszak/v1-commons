package net.ignaszak.manager.commons.restcriteria.filtering

class FilterProcessorException(message: String, cause: Throwable?) : Exception(message, cause) {
    constructor(message: String): this(message, null)
}