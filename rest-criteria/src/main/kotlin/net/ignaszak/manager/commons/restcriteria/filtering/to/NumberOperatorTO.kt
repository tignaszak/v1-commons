package net.ignaszak.manager.commons.restcriteria.filtering.to

class NumberOperatorTO(first: OperatorTO<Set<Int>>?, second: OperatorTO<Set<Int>>?) : BetweenOperatorTO<Set<Int>>(first, second)