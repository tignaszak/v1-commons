package net.ignaszak.manager.commons.restcriteria.paging.to

data class PageCriteriaTO(val page: Int)
