package net.ignaszak.manager.commons.restcriteria.filtering.to

open class BetweenOperatorTO<T>(val first: OperatorTO<T>?, val second: OperatorTO<T>?)