package net.ignaszak.manager.commons.restcriteria.filtering.annotation

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class ParameterFilter
