package net.ignaszak.manager.commons.restcriteria.filtering

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSyntaxException
import net.ignaszak.manager.commons.utils.JsonUtils
import java.util.regex.Matcher

private const val OPERATOR_CLASS = "Operator"

private val FILTER_VARIABLE_PATTERN = "(?<param>\\w+)\\s*(?<operator>(!?::|<:|>:|!?[:<>]))\\s*(?<value>(?:\\|\\||[^|])+)".toPattern()

private val ARRAY_PATTERN = "^\\s*\\[(?<elements>.+)]\\s*$".toPattern()
private val ARRAY_ELEMENTS_PATTERN = "(?<element>(?:,,|[^,])+)".toPattern()

private val BETWEEN_PATTERN = "^(?<first>.+)(?<operator2>>:|<:|<|>)(?<second>.+)$".toPattern()

private val TYPE_NAME_PATTERN = "$OPERATOR_CLASS<(?<name>[a-zA-Z\\d_.-]+)".toPattern()

private val COLLECTION_CLASSES = setOf(Set::class.java.name, List::class.java.name)
private val COLLECTION_OPERATORS = setOf("StringOperator", "NumberOperator", "DateOperator")
private val BETWEEN_OPERATOR_NAMES = setOf("DateOperator", "NumberOperator")

class FilterProcessorImpl: IFilterProcessor {

    private val fields: MutableMap<String, FieldTO> = mutableMapOf()

    override fun <M> process(filter: String, model: Class<M>): M {
        if (filter.isBlank()) throw FilterProcessorException("Invalid filter value: '${filter}'")

        for (field in model.declaredFields) {
            val name = field.type.name
                .split(".")
                .last()
                .replace("TO$".toRegex(), "")
            var isArray = COLLECTION_CLASSES.contains(field.type.name) || COLLECTION_OPERATORS.contains(name)
            var hasOperator = COLLECTION_OPERATORS.contains(name)
            val isBetween = BETWEEN_OPERATOR_NAMES.contains(name)

            if (!hasOperator && field.type.name.endsWith(OPERATOR_CLASS)) {
                val matcher = TYPE_NAME_PATTERN.matcher(field.genericType.typeName)
                if (matcher.find()) {
                    isArray = COLLECTION_CLASSES.contains(matcher.group("name"))
                }
                hasOperator = true
            }

            fields[field.name] = FieldTO(hasOperator, isArray, isBetween)
        }

        return try {
            JsonUtils.toObject(convertToJson(filter), model)
        } catch (e: JsonSyntaxException) {
            throw FilterProcessorException("Invalid filter value: '${filter}'", e)
        }
    }

    private fun convertToJson(filter: String): String {
        val varMatcher = FILTER_VARIABLE_PATTERN.matcher(filter)
        val jsonObject = JsonObject()

        while (varMatcher.find()) {
            val param = varMatcher.group("param")
            val value = varMatcher.group("value")
            val operator = varMatcher.group("operator")

            val values = mutableListOf<String>()

            var isAdded = false

            if (fields.containsKey(param)) {
                val fieldTO = fields[param]!!

                if (!fieldTO.isBetween && setOf(":", "!:", "::", "!::").contains(operator)) {
                    processEquals(value, values)
                } else if (fieldTO.isBetween && setOf(":", "!:", ">", ">:", "<", "<:").contains(operator)) {
                    val betweenMatcher = BETWEEN_PATTERN.matcher(value)

                    var first = listOf(value)
                    var operator2: String? = null
                    var second: String? = null

                    if (betweenMatcher.find()) {
                        first = listOf(betweenMatcher.group("first"))
                        operator2 = betweenMatcher.group("operator2")
                        second = betweenMatcher.group("second")
                    } else {
                        processEquals(value, values)
                        if (values.isNotEmpty()) {
                            first = values
                        }
                    }

                    val betweenObject = JsonObject()
                    betweenObject.add("first", getJsonObject(fieldTO, first, operator))

                    if (operator2 != null && second != null) {
                        betweenObject.add("second", getJsonObject(fieldTO, listOf(second), operator2))
                    }

                    jsonObject.add(param, betweenObject)

                    isAdded = true
                } else {
                    processSingle(values, value)
                }

                if (!isAdded) {
                    val element = if (fieldTO.hasOperator) {
                        getJsonObject(fieldTO, values, operator)
                    } else {
                        getJsonElement(fieldTO.isArray, values)
                    }
                    jsonObject.add(param, element)
                }

            }
        }

        return jsonObject.toString()
    }

    private fun processEquals(value: String, values: MutableList<String>) {
        val arrayMatcher = ARRAY_PATTERN.matcher(value)
        if (arrayMatcher.find()) {
            processArray(arrayMatcher, values)
        } else {
            processSingle(values, value)
        }
    }

    private fun processSingle(values: MutableList<String>, value: String) {
        values.add(unescape(value))
    }

    private fun processArray(arrayMatcher: Matcher, values: MutableList<String>) {
        val elements = arrayMatcher.group("elements")
        val elementMatcher = ARRAY_ELEMENTS_PATTERN.matcher(elements)
        while (elementMatcher.find()) {
            values.add(
                unescape(elementMatcher.group("element"))
                    .replace(",,", ",")
            )
        }
    }

    private fun getJsonObject(
        fieldTO: FieldTO,
        values: List<String>,
        operator: String
    ): JsonObject {
        val jsonObject = JsonObject()

        jsonObject.addProperty("type", operator)
        jsonObject.add("value", getJsonElement(fieldTO.isArray, values))

        return jsonObject
    }

    private fun getJsonElement(
        isArray: Boolean,
        values: List<String>
    ): JsonElement {
        val element: JsonElement

        if (isArray) {
            element = JsonArray()
            values.forEach(element::add)
        } else if (values.isNotEmpty()) {
            element = JsonPrimitive(values[0])
        } else {
            element = JsonNull.INSTANCE
        }

        return element
    }

    private fun unescape(value: String) = value.replace("||", "|")

    private data class FieldTO(val hasOperator: Boolean, val isArray: Boolean, val isBetween: Boolean)
}