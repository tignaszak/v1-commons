package net.ignaszak.manager.commons.restcriteria.paging.to

data class PageTO(val number: Int, val size: Int, val totalPages: Int, val totalElements: Long)
