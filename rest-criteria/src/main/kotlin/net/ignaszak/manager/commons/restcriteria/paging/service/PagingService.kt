package net.ignaszak.manager.commons.restcriteria.paging.service

import net.ignaszak.manager.commons.restcriteria.paging.query.PageQuery
import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO
import net.ignaszak.manager.commons.restcriteria.paging.to.PageTO

interface PagingService {
    fun buildPageTO(totalElements: Long, pageCriteriaTO: PageCriteriaTO): PageTO
    fun buildPageQuery(pageCriteriaTO: PageCriteriaTO): PageQuery
}