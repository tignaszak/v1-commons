package net.ignaszak.manager.commons.restcriteria.service

import net.ignaszak.manager.commons.restcriteria.paging.service.PagingService
import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO

class CriteriaServiceImpl(private val pagingService: PagingService): CriteriaService {
    override fun <F> buildCriteriaQuery(criteriaTO: CriteriaTO<F>): CriteriaQuery<F> {
        val pageQuery = pagingService.buildPageQuery(criteriaTO.pageCriteriaTO)

        return CriteriaQuery(pageQuery, criteriaTO.filterTO)
    }
}