package net.ignaszak.manager.commons.restcriteria.filtering.to

open class OperatorTO<V>(val type: Type, val value: V) {
    enum class Type(val character: String) {
        EQUALS(":"),
        NOT_EQUALS("!:"),
        CONTAINS("::"),
        NOT_CONTAINS("!::"),
        GREATER(">"),
        GREATER_OR_EQUALS(">:"),
        LOWER("<"),
        LOWER_OR_EQUALS("<:")
    }
}
