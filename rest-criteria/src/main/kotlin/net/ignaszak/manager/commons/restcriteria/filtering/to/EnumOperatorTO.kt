package net.ignaszak.manager.commons.restcriteria.filtering.to

class EnumOperatorTO<E: Enum<E>>(type: Type, value: Set<E>) : OperatorTO<Set<E>>(type, value)