package net.ignaszak.manager.commons.restcriteria.service

import net.ignaszak.manager.commons.restcriteria.query.CriteriaQuery
import net.ignaszak.manager.commons.restcriteria.to.CriteriaTO

interface CriteriaService {
    fun <F> buildCriteriaQuery(criteriaTO: CriteriaTO<F>) : CriteriaQuery<F>
}