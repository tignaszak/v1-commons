package net.ignaszak.manager.commons.restcriteria.filtering.to

class StringOperatorTO(type: Type, value: Set<String>) : OperatorTO<Set<String>>(type, value)
