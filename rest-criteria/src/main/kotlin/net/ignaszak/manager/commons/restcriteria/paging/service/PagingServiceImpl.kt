package net.ignaszak.manager.commons.restcriteria.paging.service

import net.ignaszak.manager.commons.restcriteria.paging.PageBuilder
import net.ignaszak.manager.commons.restcriteria.paging.query.PageQuery
import net.ignaszak.manager.commons.restcriteria.paging.to.PageCriteriaTO
import net.ignaszak.manager.commons.restcriteria.paging.to.PageTO

class PagingServiceImpl(private val pageSize: Int) : PagingService {

    override fun buildPageTO(totalElements: Long, pageCriteriaTO: PageCriteriaTO): PageTO {
        return PageBuilder()
            .totalElements(totalElements)
            .pageNumber(pageCriteriaTO.page)
            .pageSize(pageSize)
            .build()
    }

    override fun buildPageQuery(pageCriteriaTO: PageCriteriaTO) =
        PageQuery(pageSize, formatPage(pageCriteriaTO.page) * pageSize)

    private fun formatPage(page: Int) = if (page > 0) page - 1 else 0
}