package net.ignaszak.manager.commons.restcriteria.paging

import net.ignaszak.manager.commons.restcriteria.paging.to.PageTO
import kotlin.math.ceil

class PageBuilder {
    private var pageNumber: Int? = null
    private var pageSize: Int? = null
    private var totalElements: Long? = null

    fun pageNumber(pageNumber: Int) = apply { this.pageNumber = pageNumber }
    fun pageSize(pageSize: Int) = apply { this.pageSize = pageSize }
    fun totalElements(totalElements: Long) = apply { this.totalElements = totalElements }

    fun build(): PageTO {
        try {
            val totalPages = ceil(totalElements!!.toDouble() / pageSize!!).toInt()
            return PageTO(pageNumber!!, pageSize!!, totalPages, totalElements!!)
        } catch (e : NullPointerException) {
            throw PagingException("Incorrect data!", e)
        }
    }
}