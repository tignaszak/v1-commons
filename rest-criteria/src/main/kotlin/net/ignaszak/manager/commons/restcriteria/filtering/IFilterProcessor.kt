package net.ignaszak.manager.commons.restcriteria.filtering

interface IFilterProcessor {
    fun <M> process(filter: String, model: Class<M>) : M
}