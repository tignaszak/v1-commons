package net.ignaszak.manager.commons.restcriteria.paging.query

data class PageQuery(val size: Int, val offset: Int)
