package net.ignaszak.manager.commons.restcriteria.query

import net.ignaszak.manager.commons.restcriteria.paging.query.PageQuery

data class CriteriaQuery<F>(val pageQuery: PageQuery, val filterTO: F)
