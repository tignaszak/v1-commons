package net.ignaszak.manager.commons.restcriteria.filtering.to

import java.time.LocalDateTime

class DateOperatorTO(first: OperatorTO<Set<LocalDateTime>>?, second: OperatorTO<Set<LocalDateTime>>?) : BetweenOperatorTO<Set<LocalDateTime>>(first, second)