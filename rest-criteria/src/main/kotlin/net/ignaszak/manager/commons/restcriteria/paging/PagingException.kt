package net.ignaszak.manager.commons.restcriteria.paging

class PagingException(message: String, cause: Throwable?) : Exception(message, cause)