plugins {
    `maven-publish`
}

dependencies {
    implementation(project(":utils"))

    implementation(Conf.Deps.GSON)

    testImplementation(project(":rest"))
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.REST_CRITERIA
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + "manager/commons/rest-criteria") {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}