package net.ignaszak.manager.commons.querydsl.criteria

import com.querydsl.core.BooleanBuilder
import com.querydsl.core.types.dsl.DateTimePath
import com.querydsl.core.types.dsl.EnumPath
import com.querydsl.core.types.dsl.StringPath
import com.querydsl.jpa.JPQLQuery
import net.ignaszak.manager.commons.restcriteria.filtering.to.DateOperatorTO
import net.ignaszak.manager.commons.restcriteria.filtering.to.EnumOperatorTO
import net.ignaszak.manager.commons.restcriteria.filtering.to.OperatorTO
import net.ignaszak.manager.commons.restcriteria.filtering.to.OperatorTO.Type.*
import net.ignaszak.manager.commons.restcriteria.filtering.to.StringOperatorTO
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.function.Supplier
import java.util.stream.Collectors

object CriteriaHelper {

    @JvmStatic
    fun publicIdCriteria(query: JPQLQuery<*>, operator: StringOperatorTO?, supplier: Supplier<StringPath>) {
        operator?.let {
            if (it.value.size == 1) {
                when (it.type) {
                    EQUALS     -> equals(query, it.value, supplier)
                    NOT_EQUALS -> notEqualsOrNull(query, it.value, supplier)
                    else       -> {}
                }
            } else if (it.value.size > 1) {
                when (it.type) {
                    EQUALS     -> `in`(query, it.value, supplier)
                    NOT_EQUALS -> notIn(query, it.value, supplier)
                    else       -> {}
                }
            }
        }
    }

    @JvmStatic
    fun textCriteria(query: JPQLQuery<*>, operator: StringOperatorTO?, supplier: Supplier<StringPath>) {
        operator?.let {
            if (it.value.size == 1) {
                when (it.type) {
                    EQUALS       -> equals(query, it.value, supplier)
                    NOT_EQUALS   -> notEquals(query, it.value, supplier)
                    CONTAINS     -> contains(query, it.value, supplier)
                    NOT_CONTAINS -> notContains(query, it.value, supplier)
                    else         -> {}
                }
            } else if (it.value.size > 1) {
                when (it.type) {
                    EQUALS       -> `in`(query, it.value, supplier)
                    NOT_EQUALS   -> notIn(query, it.value, supplier)
                    CONTAINS     -> contains(query, it.value, supplier)
                    NOT_CONTAINS -> notContains(query, it.value, supplier)
                    else         -> {}
                }
            }
        }
    }

    @JvmStatic
    fun dateCriteria(query: JPQLQuery<*>, operator: DateOperatorTO?, supplier: Supplier<DateTimePath<LocalDateTime>>) {
        operator?.first?.let {
            if (operator.second == null) {
                if (it.value.size == 1) {
                    val builder = BooleanBuilder()
                    when (it.type) {
                        EQUALS            -> dateEquals(builder, it.value, supplier)
                        NOT_EQUALS        -> dateNotEquals(builder, it.value, supplier)
                        GREATER           -> dateGreater(builder, it.value, supplier)
                        GREATER_OR_EQUALS -> dateGreaterOrEquals(builder, it.value, supplier)
                        LOWER             -> dateLower(builder, it.value, supplier)
                        LOWER_OR_EQUALS   -> dateLowerOrEquals(builder, it.value, supplier)
                        else              -> {}
                    }
                    query.where(builder)
                } else if (it.value.size > 1) {
                    when (it.type) {
                        EQUALS     -> dateIn(query, it.value, supplier)
                        NOT_EQUALS -> dateNotIn(query, it.value, supplier)
                        else       -> {}
                    }
                } else { /* do nothing */ }
            } else if (it.value.size == 1 && operator.second?.value?.size == 1) {
                val builder = BooleanBuilder()
                dateBetween(it, builder, supplier)
                dateBetween(operator.second!!, builder, supplier)
                query.where(builder)
            } else { /* do nothing */ }
        }
    }

    @JvmStatic
    fun <F: Enum<F>, E: Enum<E>> enumCriteria(query: JPQLQuery<*>, operator: EnumOperatorTO<F>?, supplier: Supplier<EnumPath<E>>) {
        val enums = mutableSetOf<E>()
        val enumConstants = supplier.get().type.enumConstants
        operator?.value?.forEach { value ->
            enumConstants.firstOrNull {
                it.name == value.name
            }?.let(enums::add)
        }
        if (enums.size == 1) {
            when (operator?.type) {
                EQUALS     -> query.where(supplier.get().eq(enums.first()))
                NOT_EQUALS -> query.where(supplier.get().ne(enums.first()))
                else       -> {}
            }
        } else if (enums.size > 1) {
            when (operator?.type) {
                EQUALS     -> query.where(supplier.get().`in`(enums))
                NOT_EQUALS -> query.where(supplier.get().notIn(enums))
                else       -> {}
            }
        }
    }

    private fun dateBetween(operator: OperatorTO<Set<LocalDateTime>>,
                            builder: BooleanBuilder,
                            supplier: Supplier<DateTimePath<LocalDateTime>>) {
        when (operator.type) {
            GREATER           -> dateGreater(builder, operator.value, supplier)
            GREATER_OR_EQUALS -> dateGreaterOrEquals(builder, operator.value, supplier)
            LOWER             -> dateLower(builder, operator.value, supplier)
            LOWER_OR_EQUALS   -> dateLowerOrEquals(builder, operator.value, supplier)
            else              -> {}
        }
    }

    private fun dateGreater(builder: BooleanBuilder,
                            values: Set<LocalDateTime>,
                            supplier: Supplier<DateTimePath<LocalDateTime>>) {
        values.forEach {
            if (it.toLocalTime() == LocalTime.MIN) {
                builder.and(supplier.get().after(LocalDateTime.of(it.toLocalDate(), LocalTime.MAX)))
            } else {
                builder.and(supplier.get().after(it))
            }
        }
    }

    private fun dateGreaterOrEquals(builder: BooleanBuilder,
                                    values: Set<LocalDateTime>,
                                    supplier: Supplier<DateTimePath<LocalDateTime>>) {
        values.forEach {
            builder.and(supplier.get().goe(it))
        }
    }

    private fun dateLower(builder: BooleanBuilder,
                          values: Set<LocalDateTime>,
                          supplier: Supplier<DateTimePath<LocalDateTime>>) {
        values.forEach {
            builder.and(supplier.get().before(it))
        }
    }

    private fun dateLowerOrEquals(builder: BooleanBuilder,
                                  values: Set<LocalDateTime>,
                                  supplier: Supplier<DateTimePath<LocalDateTime>>) {
        values.forEach {
            if (it.toLocalTime() == LocalTime.MIN) {
                builder.and(supplier.get().loe(LocalDateTime.of(it.toLocalDate(), LocalTime.MAX)))
            } else {
                builder.and(supplier.get().loe(it))
            }
        }
    }

    private fun dateEquals(builder: BooleanBuilder,
                           values: Set<LocalDateTime>,
                           supplier: Supplier<DateTimePath<LocalDateTime>>) {
        values.forEach {
            if (it.toLocalTime() == LocalTime.MIN) {
                builder.and(supplier.get().between(it, LocalDateTime.of(it.toLocalDate(), LocalTime.MAX)))
            } else {
                builder.and(supplier.get().eq(it))
            }
        }
    }

    private fun dateNotEquals(builder: BooleanBuilder,
                              values: Set<LocalDateTime>,
                              supplier: Supplier<DateTimePath<LocalDateTime>>) {
        values.forEach {
            if (it.toLocalTime() == LocalTime.MIN) {
                builder.and(supplier.get().notBetween(it, LocalDateTime.of(it.toLocalDate(), LocalTime.MAX)))
            } else {
                builder.and(supplier.get().ne(it))
            }
        }
    }

    private fun <T> dateIn(query: JPQLQuery<T>,
                           values: Set<LocalDateTime>,
                           supplier: Supplier<DateTimePath<LocalDateTime>>) {
        if (hasLocalDate(values)) {
            val builder = BooleanBuilder()
            values.forEach {
                builder.or(supplier.get().between(
                    LocalDateTime.of(it.toLocalDate(), LocalTime.MIN),
                    LocalDateTime.of(it.toLocalDate(), LocalTime.MAX)
                ))
            }
            query.where(builder)
        } else {
            query.where(supplier.get().`in`(values))
        }
    }

    private fun hasLocalDate(values: Set<LocalDateTime>) =
        values.stream().anyMatch { it.toLocalTime() == LocalTime.MIN }

    private fun <T> dateNotIn(query: JPQLQuery<T>,
                              values: Set<LocalDateTime>,
                              supplier: Supplier<DateTimePath<LocalDateTime>>) {

        if (hasLocalDate(values)) {
            val builder = BooleanBuilder()
            values.forEach {
                builder.and(supplier.get().notBetween(
                    LocalDateTime.of(it.toLocalDate(), LocalTime.MIN),
                    LocalDateTime.of(it.toLocalDate(), LocalTime.MAX)
                ))
            }
            query.where(builder)
        } else {
            query.where(supplier.get().notIn(values).or(supplier.get().isNull))
        }
    }

    private fun <T> equals(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        values.forEach {
            query.where(supplier.get().equalsIgnoreCase(it))
        }
    }

    private fun <T> notEquals(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        values.forEach {
            query.where(supplier.get().notEqualsIgnoreCase(it))
        }
    }

    private fun <T> notEqualsOrNull(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        values.forEach {
            query.where(supplier.get().notEqualsIgnoreCase(it).or(supplier.get().isNull))
        }
    }

    private fun <T> `in`(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        query.where(supplier.get().toLowerCase().`in`(toLowerCase(values)))
    }

    private fun <T> notIn(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        query.where(supplier.get().toLowerCase().notIn(toLowerCase(values)))
    }

    private fun <T> contains(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        val builder = BooleanBuilder()
        values.forEach {
            builder.or(supplier.get().containsIgnoreCase(it))
        }
        query.where(builder)
    }

    private fun <T> notContains(query: JPQLQuery<T>, values: Set<String>, supplier: Supplier<StringPath>) {
        val builder = BooleanBuilder()
        values.forEach {
            builder.or(supplier.get().containsIgnoreCase(it).not())
        }
        query.where(builder)
    }

    private fun toLowerCase(values: Set<String>) = values.stream()
        .map(String::lowercase)
        .collect(Collectors.toSet())
}