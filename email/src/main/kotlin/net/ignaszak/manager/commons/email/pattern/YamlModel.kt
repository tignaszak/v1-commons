package net.ignaszak.manager.commons.email.pattern

open class YamlModel {
    var registration: SimpleModel = SimpleModel()
    var resetPassword: SimpleModel = SimpleModel()

    class SimpleModel {
        var subject: String = ""
        var body: String = ""
    }
}
