package net.ignaszak.manager.commons.email.template

import freemarker.cache.StringTemplateLoader
import freemarker.template.Configuration
import net.ignaszak.manager.commons.email.config.EMAIL_FREEMARKER_CONFIGURATION_BEAN
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import java.io.StringWriter

@Component
class TemplateEngineImpl(@Qualifier(EMAIL_FREEMARKER_CONFIGURATION_BEAN) private val freeMarkerConf: Configuration): TemplateEngineInterface {

    private val templateLoaderMap: MutableMap<String, String> = HashMap()

    override fun addTemplateString(templateName: String, templateContent: String): TemplateEngineInterface {
        templateLoaderMap[templateName] = templateContent
        return this
    }

    override fun process(templateName: String, vars: Map<String, String>): String {
        addTemplateLoader()
        val template = freeMarkerConf.getTemplate(templateName)
        val writer = StringWriter()
        template.process(vars, writer)
        freeMarkerConf.templateLoader = TemplateUtils.getClassLoader()
        return writer.toString()
    }

    private fun addTemplateLoader() {
        if (templateLoaderMap.isNotEmpty()) {
            val loader = StringTemplateLoader()
            for ((name, content) in templateLoaderMap) loader.putTemplate(name, content)
            freeMarkerConf.templateLoader = TemplateUtils.getClassLoader(loader)
        }
    }
}