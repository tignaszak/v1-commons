package net.ignaszak.manager.commons.email.service

import net.ignaszak.manager.commons.email.model.EmailModel
import net.ignaszak.manager.commons.email.pattern.PatternEngineInterface
import net.ignaszak.manager.commons.email.service.validator.EmailValidatorInterface
import net.ignaszak.manager.commons.email.template.TemplateEngineInterface
import net.ignaszak.manager.commons.messaging.message.EmailMessage
import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.utils.ObjectUtils
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service
import java.util.*

@Service
class EmailServiceImpl(private val rabbitTemplate: RabbitTemplate,
                       private val patternEngine: PatternEngineInterface,
                       private val templateEngine: TemplateEngineInterface,
                       private val emailValidator: EmailValidatorInterface,
                       private val managerProperties: ManagerProperties): EmailServiceInterface {

    private companion object {
        val log: Logger = LogManager.getLogger(EmailServiceImpl::class.java)
    }

    override fun send(model: EmailModel) {
        emailValidator.valid(model)
        val pattern = patternEngine.getPattern(model.pattern)
        val vars = getVars(model)
        val body = templateEngine
            .addTemplateString("param_body.ftl", pattern.body)
             // todo: consider define template for each email type MAN-87
            .process("default.ftl", vars)
        val emailMessage = EmailMessage(model.to, managerProperties.email.from, pattern.subject, body)
        managerProperties.jms.queues["email"]?.let {
            rabbitTemplate.convertAndSend(it.exchange, it.routingKey, emailMessage)
        } ?: log.error("Queue with id 'email' is not defined!")
    }

    private fun getVars(model: EmailModel) : Map<String, String> {
        val dataMap = model.data?.let { ObjectUtils.toStringMap(it) } ?: emptyMap()

        val mutableMap = mutableMapOf("email" to model.to)
        mutableMap.putAll(dataMap)

        return Collections.unmodifiableMap(mutableMap)
    }
}