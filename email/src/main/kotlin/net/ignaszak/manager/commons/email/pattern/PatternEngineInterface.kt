package net.ignaszak.manager.commons.email.pattern

interface PatternEngineInterface {
    fun getPattern(pattern: EmailPattern): Pattern
}