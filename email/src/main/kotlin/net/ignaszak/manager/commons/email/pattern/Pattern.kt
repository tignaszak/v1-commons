package net.ignaszak.manager.commons.email.pattern

data class Pattern(val subject: String, val body: String)
