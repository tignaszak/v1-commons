package net.ignaszak.manager.commons.email.pattern

import net.ignaszak.manager.commons.email.pattern.YamlModel.SimpleModel
import kotlin.reflect.KProperty1

enum class EmailPattern(private val modelReceiver: KProperty1<YamlModel, SimpleModel>) {
    REGISTRATION(YamlModel::registration),
    RESET_PASSWORD(YamlModel::resetPassword);

    fun getLowerName(): String {
        return this.toString().lowercase()
    }

    fun getSimpleModel(yamlModel: YamlModel): SimpleModel {
        return modelReceiver.get(yamlModel)
    }
}