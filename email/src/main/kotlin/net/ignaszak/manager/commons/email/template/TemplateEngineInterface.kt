package net.ignaszak.manager.commons.email.template

interface TemplateEngineInterface {
    fun addTemplateString(templateName: String, templateContent: String): TemplateEngineInterface
    fun process(templateName: String, vars: Map<String, String>): String
}