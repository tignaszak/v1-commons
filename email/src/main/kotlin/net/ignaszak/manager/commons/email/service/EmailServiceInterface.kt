package net.ignaszak.manager.commons.email.service

import net.ignaszak.manager.commons.email.model.EmailModel

interface EmailServiceInterface {
    fun send(model: EmailModel)
}