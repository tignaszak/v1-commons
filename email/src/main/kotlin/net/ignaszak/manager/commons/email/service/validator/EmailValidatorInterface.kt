package net.ignaszak.manager.commons.email.service.validator

import net.ignaszak.manager.commons.email.model.EmailModel

interface EmailValidatorInterface {
    fun valid(emailModel: EmailModel)
}