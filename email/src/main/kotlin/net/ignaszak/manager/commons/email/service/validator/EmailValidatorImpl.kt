package net.ignaszak.manager.commons.email.service.validator

import net.ignaszak.manager.commons.utils.SecurityUtils
import net.ignaszak.manager.commons.email.model.EmailModel
import org.springframework.stereotype.Component

@Component
class EmailValidatorImpl: EmailValidatorInterface {
    override fun valid(emailModel: EmailModel) {
        if (SecurityUtils.isNotEmail(emailModel.to)) throw EmailValidationException(emailModel)
    }
}