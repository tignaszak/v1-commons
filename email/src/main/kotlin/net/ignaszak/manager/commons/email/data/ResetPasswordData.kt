package net.ignaszak.manager.commons.email.data

data class ResetPasswordData(val link: String)
