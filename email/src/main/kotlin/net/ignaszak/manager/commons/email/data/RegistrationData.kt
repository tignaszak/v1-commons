package net.ignaszak.manager.commons.email.data

data class RegistrationData(val activationCode: String)
