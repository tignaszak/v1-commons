package net.ignaszak.manager.commons.email.pattern

import org.springframework.stereotype.Component
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor

@Component
class PatternEngineImpl: PatternEngineInterface {

    override fun getPattern(pattern: EmailPattern): Pattern {
        val yamlModel = getYamlModel()
        val simpleModel = pattern.getSimpleModel(yamlModel)
        return Pattern(simpleModel.subject, simpleModel.body)
    }

    private fun getYamlModel(): YamlModel {
        javaClass.getResourceAsStream("/patterns/en_US.yml").use {
            val yaml = Yaml(Constructor(YamlModel::class.java))
            return yaml.load(it)
        }
    }
}