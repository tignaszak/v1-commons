package net.ignaszak.manager.commons.email.model

import net.ignaszak.manager.commons.email.pattern.EmailPattern

data class EmailModel(val to: String, val pattern: EmailPattern, val data: Any?) {
    constructor(to: String, pattern: EmailPattern) : this(to, pattern, null)
}
