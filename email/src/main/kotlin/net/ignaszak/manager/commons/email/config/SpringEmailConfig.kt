package net.ignaszak.manager.commons.email.config

import net.ignaszak.manager.commons.email.template.TemplateUtils
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


const val EMAIL_ASPECT_EXECUTOR_SERVICE_BEAN = "emailAspectExecutorService"
const val EMAIL_FREEMARKER_CONFIGURATION_BEAN = "emailFreemarkerConfiguration"

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("net.ignaszak.manager.commons.email")
open class SpringEmailConfig {

    @Bean(EMAIL_ASPECT_EXECUTOR_SERVICE_BEAN)
    open fun emailExecutorService(): ExecutorService {
        return Executors.newSingleThreadExecutor()
    }

    @Bean(EMAIL_FREEMARKER_CONFIGURATION_BEAN)
    open fun emailFreeMarkerConfiguration(): freemarker.template.Configuration {
        val conf = freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_29)
        conf.defaultEncoding = "utf-8"
        conf.templateLoader = TemplateUtils.getClassLoader()
        return conf
    }
}
