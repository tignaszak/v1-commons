package net.ignaszak.manager.commons.email.annotation

import net.ignaszak.manager.commons.email.pattern.EmailPattern

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class SimpleEmail(
    val value: EmailPattern
)