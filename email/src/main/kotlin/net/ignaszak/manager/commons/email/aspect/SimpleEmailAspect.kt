package net.ignaszak.manager.commons.email.aspect

import net.ignaszak.manager.commons.email.annotation.EmailTo
import net.ignaszak.manager.commons.email.annotation.SimpleEmail
import net.ignaszak.manager.commons.email.config.EMAIL_ASPECT_EXECUTOR_SERVICE_BEAN
import net.ignaszak.manager.commons.email.model.EmailModel
import net.ignaszak.manager.commons.email.pattern.EmailPattern
import net.ignaszak.manager.commons.email.service.EmailServiceInterface
import net.ignaszak.manager.commons.rest.response.AdditionalData
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.Response
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.reflect.MethodSignature
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import java.util.concurrent.ExecutorService

@Aspect
@Component
class SimpleEmailAspect(private val emailService: EmailServiceInterface,
                        @Qualifier(EMAIL_ASPECT_EXECUTOR_SERVICE_BEAN) private val executorService: ExecutorService) {

    companion object {
        private val logger: Logger = LogManager.getLogger()
    }

    @AfterReturning(
        pointcut = "@annotation(net.ignaszak.manager.commons.email.annotation.SimpleEmail)",
        returning = "model"
    )
    fun sendEmail(joinPoint: JoinPoint, model: Response) {
        try {
            executorService.submit {
                val methodSignature = joinPoint.signature as MethodSignature
                val annotation = methodSignature.method.getAnnotation(SimpleEmail::class.java)
                val emailModel = getEmailModel(model, annotation.value)

                emailService.send(emailModel)
            }
        } catch (e: Throwable) {
            logger.warn("Could not send email message", e)
        }
    }

    private fun getEmailModel(model: Response, pattern: EmailPattern): EmailModel {

        val to = extractEmailAddress(model)

        return if (model is AdditionalData)
            model.additionalData?.let {
                EmailModel(to, pattern, it)
            } ?: EmailModel(to, pattern)
        else EmailModel(to, pattern)
    }

    private fun extractEmailAddress(model: Any?) : String {
        return when (model) {
            is DataResponse<*> -> getEmailAddressFromAnnotatedField(model.data)
            else               -> ""
        }
    }

    private fun getEmailAddressFromAnnotatedField(model: Any?) : String {
        var to = ""

        val fields = model?.javaClass?.declaredFields ?: emptyArray()

        for (field in fields) {
            field.trySetAccessible()
            for (annotation in field.declaredAnnotations) {
                if (annotation is EmailTo) {
                    val emailToValue = field.get(model)
                    if (emailToValue is String) {
                        to = emailToValue
                        break
                    }
                }
            }
        }

        return to
    }
}