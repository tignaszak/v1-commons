package net.ignaszak.manager.commons.email.template

import freemarker.cache.ClassTemplateLoader
import freemarker.cache.MultiTemplateLoader
import freemarker.cache.TemplateLoader

object TemplateUtils {

    fun getClassLoader(): TemplateLoader {
        return ClassTemplateLoader(javaClass, "/templates/")
    }

    fun getClassLoader(templateLoader: TemplateLoader): TemplateLoader {
        val loaders = arrayOf(getClassLoader(), templateLoader)
        return MultiTemplateLoader(loaders)
    }
}