package net.ignaszak.manager.commons.email.service.validator

import net.ignaszak.manager.commons.email.model.EmailModel
import java.lang.Exception

class EmailValidationException(emailModel: EmailModel): Exception("Invalid email model: $emailModel")
