package net.ignaszak.manager.commons.email.aspect

import net.ignaszak.manager.commons.email.annotation.SimpleEmail
import net.ignaszak.manager.commons.email.pattern.EmailPattern
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.Response

import static net.ignaszak.manager.commons.email.aspect.TestUtils.anyRegistrationData

@SuppressWarnings("all")
class MockController {

    @SimpleEmail(EmailPattern.REGISTRATION)
    Response returnMockModelAnnotation() {
        return new DataResponse<>(new TestUtils.MockModelAnnotation())
    }

    @SimpleEmail(EmailPattern.REGISTRATION)
    Response returnMockModelAnnotationEmpty() {
        return new DataResponse<>(new TestUtils.MockModelAnnotationEmpty())
    }

    @SimpleEmail(EmailPattern.REGISTRATION)
    Response returnMockModel() {
        return new DataResponse<>(new TestUtils.MockModel())
    }

    @SimpleEmail(EmailPattern.REGISTRATION)
    Response returnMockModelAnnotatedAdditionalData() {
        return new DataResponse<>(new TestUtils.MockModelAnnotation(), anyRegistrationData())
    }

    Response returnMockModelAnnotatedAdditionalDataWithoutSimpleEmail() {
        return new DataResponse<>(new TestUtils.MockModelAnnotation(), anyRegistrationData())
    }
}
