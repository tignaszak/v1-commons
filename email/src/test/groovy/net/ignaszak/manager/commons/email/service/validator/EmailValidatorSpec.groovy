package net.ignaszak.manager.commons.email.service.validator

import net.ignaszak.manager.commons.email.model.EmailModel
import net.ignaszak.manager.commons.email.pattern.EmailPattern
import spock.lang.Specification

class EmailValidatorSpec extends Specification {

    private EmailValidatorInterface validator = new EmailValidatorImpl()

    def "no exceptions are thrown if model is valid"() {
        given:
        def model = anyEmailModel()

        when:
        validator.valid(model)

        then:
        noExceptionThrown()
    }

    def "an exception is thrown when email is invalid"() {
        given:
        def model = invalidEmailModel()

        when:
        validator.valid(model)

        then:
        def e = thrown(EmailValidationException)
        e.message == "Invalid email model: " + model
    }

    private static EmailModel anyEmailModel() {
        new EmailModel("to@to.to", EmailPattern.REGISTRATION)
    }

    private static EmailModel invalidEmailModel() {
        new EmailModel("invalid@email", EmailPattern.REGISTRATION)
    }
}
