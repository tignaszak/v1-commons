package net.ignaszak.manager.commons.email.pattern

import spock.lang.Specification

class PatternEngineSpec extends Specification {

    def "test engine"() {
        given:
        def emailPattern = EmailPattern.REGISTRATION
        def engine = new PatternEngineImpl()

        when:
        def pattern = engine.getPattern(emailPattern)

        then:
        pattern.subject == "Welcome"
        pattern.body == "<b>Welcome \${email} to manager app</b>"
    }
}
