package net.ignaszak.manager.commons.email.template

import freemarker.template.Configuration
import net.ignaszak.manager.commons.email.config.SpringEmailConfig
import spock.lang.Specification

class TemplateEngineSpec extends Specification {

    private Configuration confStub = new SpringEmailConfig().emailFreeMarkerConfiguration()

    def "test engine"() {
        given:
        def templateEngine = new TemplateEngineImpl(confStub)

        when:
        templateEngine.addTemplateString("templateString1.ftl", "\${var1}")
        templateEngine.addTemplateString("templateString2.ftl", "\${var2}")
        def result = templateEngine.process("rootTemplate.ftl", [var1: "value1", var2: "value2", var3: "value3"])

        then:
        result == "value1, value2, value3"
    }
}
