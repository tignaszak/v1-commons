package net.ignaszak.manager.commons.email.service

import net.ignaszak.manager.commons.conf.properties.ManagerProperties
import net.ignaszak.manager.commons.email.data.RegistrationData
import net.ignaszak.manager.commons.email.model.EmailModel
import net.ignaszak.manager.commons.email.pattern.EmailPattern
import net.ignaszak.manager.commons.email.pattern.Pattern
import net.ignaszak.manager.commons.email.pattern.PatternEngineInterface
import net.ignaszak.manager.commons.email.service.validator.EmailValidatorInterface
import net.ignaszak.manager.commons.email.template.TemplateEngineInterface
import net.ignaszak.manager.commons.messaging.message.EmailMessage
import net.ignaszak.manager.commons.test.ManagerPropertiesTest
import org.springframework.amqp.rabbit.core.RabbitTemplate
import spock.lang.Specification

class EmailServiceSpec extends Specification {

    private static final String ANY_EMAIL = "to@to.to"
    private static final String ANY_BODY = "body"
    private static final String ANY_SUBJECT = "subject"
    private static final String ANY_ACTIVATION_CODE = "1234"

    private EmailServiceInterface service

    private RabbitTemplate rabbitTemplateMock = Mock()
    private PatternEngineInterface patternEngineMock = Mock()
    private TemplateEngineInterface templateEngineMock = Mock()
    private EmailValidatorInterface emailValidatorMock = Mock()

    private ManagerProperties managerProperties = ManagerPropertiesTest.getManagerProperties()

    def setup() {
        service = new EmailServiceImpl(rabbitTemplateMock, patternEngineMock, templateEngineMock, emailValidatorMock, managerProperties)
    }

    def "send registration email message"() {
        given:
        patternEngineMock.getPattern(EmailPattern.REGISTRATION) >> anyPattern()
        templateEngineMock.addTemplateString(_ as String, _ as String) >> templateEngineMock
        def emailModel = anyEmailModel()

        when:
        service.send(emailModel)

        then:
        1 * templateEngineMock.process(_ as String, {
            it.containsKey("activationCode")
            it.containsValue(ANY_ACTIVATION_CODE)
            it.containsKey("email")
            it.containsValue(ANY_EMAIL)
        }) >> ANY_BODY

        1 * rabbitTemplateMock.convertAndSend(managerProperties.jms.queues["email"].exchange, managerProperties.jms.queues["email"].routingKey, _ as EmailMessage) >> {
            assert it.get(2).to == ANY_EMAIL
            assert it.get(2).from == managerProperties.email.from
            assert it.get(2).subject == ANY_SUBJECT
            assert it.get(2).body == ANY_BODY
        }
    }

    private static EmailModel anyEmailModel() {
        new EmailModel(ANY_EMAIL, EmailPattern.REGISTRATION, new RegistrationData(ANY_ACTIVATION_CODE))
    }

    private static Pattern anyPattern() {
        new Pattern(ANY_SUBJECT, ANY_BODY)
    }
}
