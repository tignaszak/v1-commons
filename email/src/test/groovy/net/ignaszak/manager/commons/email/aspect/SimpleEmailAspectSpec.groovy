package net.ignaszak.manager.commons.email.aspect

import net.ignaszak.manager.commons.email.pattern.EmailPattern
import net.ignaszak.manager.commons.email.service.EmailServiceInterface
import net.ignaszak.manager.commons.rest.response.AdditionalData
import net.ignaszak.manager.commons.rest.response.DataResponse
import net.ignaszak.manager.commons.rest.response.Response
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory
import spock.lang.Specification

import java.util.concurrent.ExecutorService

import static net.ignaszak.manager.commons.email.aspect.TestUtils.ANY_EMAIL
import static net.ignaszak.manager.commons.email.aspect.TestUtils.anyRegistrationData

class SimpleEmailAspectSpec extends Specification {
    SimpleEmailAspect aspect

    EmailServiceInterface emailServiceMock = Mock()

    def setup() {
        def executorServiceMock = Mock(ExecutorService)
        executorServiceMock.submit(_ as Runnable) >> {arguments -> arguments.get(0).run()}

        this.aspect = new SimpleEmailAspect(emailServiceMock, executorServiceMock)
    }

    def "should send email if received model has not empty annotated field"() {
        given:
        def proxy = getMockControllerProxy()

        when:
        Response response = proxy.returnMockModelAnnotation()

        then:
        response instanceof DataResponse
        1 * emailServiceMock.send({
            it.to == ANY_EMAIL
            it.pattern == EmailPattern.REGISTRATION
        })
    }

    def "should send email if received model has empty annotated field"() {
        given:
        def proxy = getMockControllerProxy()

        when:
        Response response = proxy.returnMockModelAnnotationEmpty()

        then:
        response instanceof DataResponse
        1 * emailServiceMock.send({
            it.to == ""
            it.pattern == EmailPattern.REGISTRATION
        })
    }

    def "should send email if received model has any annotated field"() {
        given:
        def proxy = getMockControllerProxy()

        when:
        Response response = proxy.returnMockModel()

        then:
        response instanceof DataResponse
        1 * emailServiceMock.send({
            it.to == ""
            it.pattern == EmailPattern.REGISTRATION
        })
    }

    def "should send email with additional data if received model has not empty annotated field"() {
        given:
        def proxy = getMockControllerProxy()

        when:
        Response response = proxy.returnMockModelAnnotatedAdditionalData()

        then:
        response instanceof AdditionalData
        1 * emailServiceMock.send({
            it.to == ANY_EMAIL
            it.pattern == EmailPattern.REGISTRATION
            it.data == anyRegistrationData()
        })
    }

    def "should return only data type when simple email wrapper is set and no email annotation is added"() {
        given:
        def proxy = getMockControllerProxy()

        when:
        Response response = proxy.returnMockModelAnnotatedAdditionalDataWithoutSimpleEmail()

        then:
        response instanceof AdditionalData
        0 * emailServiceMock.send(_)
    }

    private MockController getMockControllerProxy() {
        def mockController = new MockController()
        def factory = new AspectJProxyFactory()
        factory.setTarget(mockController)
        factory.addAspect(aspect)
        return (MockController) factory.getProxy()
    }
}
