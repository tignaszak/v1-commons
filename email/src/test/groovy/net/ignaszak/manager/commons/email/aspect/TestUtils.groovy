package net.ignaszak.manager.commons.email.aspect

import net.ignaszak.manager.commons.email.annotation.EmailTo
import net.ignaszak.manager.commons.email.data.RegistrationData

class TestUtils {

    static final String ANY_EMAIL = "test@email.pl"

    static class MockModelAnnotation {
        @EmailTo
        private email = ANY_EMAIL
    }

    static class MockModelAnnotationEmpty {
        @EmailTo
        private email = ""
    }

    static class MockModel {
        private email = ANY_EMAIL
    }

    static RegistrationData anyRegistrationData() {
        new RegistrationData("1234")
    }

    private TestUtils() {}
}
