plugins {
    `maven-publish`
}

dependencies {
    implementation(project(":conf"))
    implementation(project(":messaging"))
    implementation(project(":rest"))
    implementation(project(":utils"))

    testImplementation(project(":test"))

    implementation(Conf.Deps.SPRING_BOOT_STARTER_AMQP)
    implementation(Conf.Deps.SPRING_BOOT_STARTER_AOP)
    implementation(Conf.Deps.SPRING_FRAMEWORK_CONTEXT)

    implementation(Conf.Deps.ASPECTJ_ASPECTJRT)
    implementation(Conf.Deps.ASPECTJ_ASPECTJWEAVER)
    implementation(Conf.Deps.FREEMARKER)
    implementation(Conf.Deps.LOG4J_API)
    implementation(Conf.Deps.SNAKEYAML)
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.EMAIL
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + Conf.Modules.EMAIL.replace("-", "/")) {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}