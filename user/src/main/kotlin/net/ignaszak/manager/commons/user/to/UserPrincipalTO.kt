package net.ignaszak.manager.commons.user.to

data class UserPrincipalTO(val publicId: String, val email: String)
