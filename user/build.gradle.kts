plugins {
    `maven-publish`
}

dependencies {
    implementation(Conf.Deps.SWAGGER_ANNOTATIONS)
}

publishing {
    repositories {
        maven {
            url = uri("${project.rootDir}/../${Conf.LOCAL_MAVEN_REPOSITORY}")
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = Conf.GROUP
            artifactId = Conf.Modules.USER
            version = Conf.Versions.MANAGER_COMMONS
            from(components["java"])
            artifact(tasks["sourceJar"])
        }
    }
}

tasks.jar {
    into(Conf.META_INFO_PATH + Conf.Modules.USER.replace("-", "/")) {
        from(Conf.POM_DEFAULT_PATH)
        rename(".*", "pom.xml")
    }
}