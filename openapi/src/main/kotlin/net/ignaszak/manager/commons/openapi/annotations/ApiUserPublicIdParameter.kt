package net.ignaszak.manager.commons.openapi.annotations

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn.PATH

@Parameter(description = "User public id", required = true, `in` = PATH, example = "f75a505e-0a63-464d-9211-fdd17693b00c")
annotation class ApiUserPublicIdParameter
