package net.ignaszak.manager.commons.openapi.annotations

import io.swagger.v3.oas.annotations.security.SecurityRequirement

@SecurityRequirement(name = "User JWT")
annotation class UserJwtSecurityRequirement