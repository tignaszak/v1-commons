package net.ignaszak.manager.commons.openapi.annotations

import io.swagger.v3.oas.annotations.responses.ApiResponse

@ApiResponse(responseCode = "201")
annotation class ApiCreatedResponse
