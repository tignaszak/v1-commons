package net.ignaszak.manager.commons.openapi.annotations

import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import net.ignaszak.manager.commons.rest.response.ErrorResponse

@ApiResponse(
    responseCode = "403",
    content = [
        Content(mediaType = "application/json", schema = Schema(implementation = ErrorResponse::class))
    ]
)
annotation class ApiForbiddenResponse
