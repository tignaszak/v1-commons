package net.ignaszak.manager.commons.openapi

import io.swagger.v3.oas.models.media.Content
import io.swagger.v3.oas.models.media.IntegerSchema
import io.swagger.v3.oas.models.media.MediaType
import io.swagger.v3.oas.models.media.Schema
import io.swagger.v3.oas.models.media.StringSchema
import io.swagger.v3.oas.models.parameters.Parameter
import io.swagger.v3.oas.models.responses.ApiResponse
import io.swagger.v3.oas.models.responses.ApiResponses
import net.ignaszak.manager.commons.rest.page.PageResponse
import net.ignaszak.manager.commons.rest.response.ListResponse

object OpenApiUtils {

    @JvmStatic
    fun getOKListResponse(response: Any): ApiResponses {
        val schema = Schema<ListResponse<*>>()
        schema.setDefault(
            ListResponse(
                PageResponse(0, 0, 0, 0),
                listOf(response)
            )
        )

        val content = Content()
            .addMediaType(
                "application/json",
                MediaType().schema(schema)
            )

        return ApiResponses().addApiResponse(
            "200",
            ApiResponse()
                .description("OK")
                .content(content)
        )
    }

    @JvmStatic
    fun getPageParameter() = Parameter()
        .`in`("query")
        .schema(IntegerSchema())
        .required(false)
        .name("page")
        .description("Page number")

    @JvmStatic
    fun getFilterParameter(description: String) = Parameter()
        .`in`("query")
        .schema(StringSchema())
        .required(false)
        .name("filter")
        .description(description)
}