package net.ignaszak.manager.commons.openapi.description

object CriteriaDescription {
    const val PUBLIC_ID_OPERATORS = "allowed operators <code>:  !:</code>"
    const val TEXT_OPERATORS = "allowed operators <code>:  !:  ::  !::</code>"
    const val DATE_OPERATORS = "allowed operators <code>:  !:  &lt;  &lt;:  &gt;  &gt;:</code>"
}