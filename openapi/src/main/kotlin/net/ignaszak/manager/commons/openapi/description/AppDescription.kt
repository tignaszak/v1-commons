package net.ignaszak.manager.commons.openapi.description

object AppDescription {
    const val CRITERIA_DESCRIPTION =
                "<strong>Pagination</strong>\n\nApplied to list endpoints using query param <code>page</code> with page number." +
                "\n\n<strong>Filtering</strong>\n\nCan be applied to every list endpoints using query parameter <code>filter</code>. " +
                "Filter scheme: <code>&lt;parameter name&gt;&lt;operator&gt;&lt;value&gt;|</code>:" +
                "\n1. <code>&lt;parameter name&gt;</code> - name of filtered parameter" +
                "\n2. <code>|</code> - filter separator, works as <strong>and</strong> operator. " +
                      "If its used as value must be escaped by <code>|</code> character, example: <code>test || test</code> will be treated as <code>test | test </code>." +
                "\n3. <code>value</code> - searched value, must be url encoded, types:" +
                "\n   - <strong>array</strong> - allowed for equals (<code>:</code>), not equals (<code>!:</code>), contains (<code>::</code>) and not contains (<code>!::</code>) operators. " +
                        "Array elements must be defined inside bracksets <code>[]</code>. Each element must be comma separated <code>,</code>. " +
                        "To use comma as a normal character, it must be escaped with comma eg: <code>[test1,,test2,test3]</code> will be treated as 2 elements: " +
                        "<code>test1,test2</code> and <code>test3</code>. Array type example: <code>param:[value1,value2,value3]</code>." +
                "\n   - <strong>text</strong> - requires only <code>|</code> to be escaped, allowed for equals (<code>:</code>), not equals (<code>!:</code>), contains (<code>::</code>) and not contains (<code>!::</code>) operators. Text can be used in array." +
                "\n   - <strong>number</strong> - allowed for equals (<code>:</code>), not equals (<code>!:</code>), greater (<code>&lt;  &lt;:</code>) and lower (<code>&gt;  &gt;:</code>) operators. Can be used in array (equals or not equals). " +
                        "Numbers could be filtered in range (greater or lower): <code>number&gt;5&lt;:15|</code>" +
                "\n   - <strong>data</strong> - format: <code>YYYY-MM-DD'T'hh:mm:ss</code>, allowed for equals (<code>:</code>), not equals (<code>!:</code>), greater (<code>&lt;  &lt;:</code>) and lower (<code>&gt;  &gt;:</code>) operators. " +
                        "Date could be filtered in range (greater or lower): <code>date&gt;2021-01-13T17:09:4&lt;:2022-01-13T17:09:4|</code>" +
                "\n4. <code>&lt;operator&gt;</code> operator describing how parameter should be filtered. All operators are case insensitive. Possible operators:" +
                "\n   - <code>:</code> - <strong>in</strong>, one or more comma seperated values" +
                "\n   - <code>!:</code> - <strong>not in</strong>, one or more comma seperated values" +
                "\n   - <code>::</code> - <strong>contains</strong>" +
                "\n   - <code>!::</code> - <strong>not contains</strong>" +
                "\n   - <code>&lt;</code> - <strong>greater than</strong>" +
                "\n   - <code>&lt;:</code> - <strong>greater or equals than</strong>" +
                "\n   - <code>&gt;</code> - <strong>lower than</strong>" +
                "\n   - <code>&gt;:</code> - <strong>lower or equals than</strong>" +
                "\n\nExample: <code>authorPublicId::[value1,value2]|text:example text|creationDate&gt;2022-01-13T17:09:42</code>."
}