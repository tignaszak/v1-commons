import org.gradle.api.Project
import org.gradle.kotlin.dsl.*

object Conf {

    val LOCAL_MAVEN_REPOSITORY = "maven-repository/"

    const val GROUP = "net.ignaszak"
    const val VERSION = "1.0.0-SNAPSHOT"

    const val POM_DEFAULT_PATH = "build/publications/maven/pom-default.xml"
    const val META_INFO_PATH = "META-INF/maven/net/ignaszak/"

    object Modules {
        const val CONF = "manager-commons-conf"
        const val EMAIL = "manager-commons-email"
        const val ERROR = "manager-commons-error"
        const val HTTP_CLIENT = "manager-commons-http-client"
        const val JWT = "manager-commons-jwt"
        const val MESSAGING = "manager-commons-messaging"
        const val OPENAPI = "manager-commons-openapi"
        const val QUERYDSL = "manager-commons-querydsl"
        const val REST = "manager-commons-rest"
        const val REST_CRITERIA = "manager-commons-rest-criteria"
        const val SPRING = "manager-commons-spring"
        const val TEST = "manager-commons-test"
        const val USER = "manager-commons-user"
        const val UTILS = "manager-commons-utils"
    }

    object Versions {
        const val MANAGER_COMMONS = "master-SNAPSHOT"

        const val KOTLIN = "1.7.20"
        const val JAVA = "18"

        const val SPRING_BOOT = "2.7.5"
        const val SPRING_FRAMEWORK = "5.3.23"
        const val SPRING_SECURITY = "5.7.5"

        const val APACHE_HTTPCLIENT = "5.2-beta1"
        const val ARCHUNIT = "1.0.0"
        const val ASPECTJRT = "1.9.9.1"
        const val FREEMARKER = "2.3.31"
        const val GROOVY = "4.0.6"
        const val GSON = "2.10"
        const val JACKSON = "2.13.4"
        const val JJWT = "0.11.5"
        const val LOG4J = "2.19.0"
        const val MAPSTRUCT = "1.5.3.Final"
        const val OPENAPI = "1.6.12"
        const val QUERYDSL = "5.0.0"
        const val SNAKEYAML = "1.33"
        const val SPOCK_SPRING = "2.3-groovy-4.0"
        const val SWAGGER = "2.2.6"
        const val TESTCONTAINERS = "1.17.5"
    }

    object Deps {
        // Kotlin
        const val KOTLIN_REFLECT = "org.jetbrains.kotlin:kotlin-reflect"
        const val KOTLIN_STDLIN_JDK8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
        const val STDLIB = "stdlib"
        // Spock
        const val GROOVY = "org.apache.groovy:groovy:${Versions.GROOVY}"
        const val SPOCK_CORE = "org.spockframework:spock-core:${Versions.SPOCK_SPRING}"
        const val SPOCK_SPRING = "org.spockframework:spock-spring:${Versions.SPOCK_SPRING}"
        // Spring
        const val SPRING_BOOT = "org.springframework.boot:spring-boot:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_AMQP = "org.springframework.boot:spring-boot-starter-amqp:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_AOP = "org.springframework.boot:spring-boot-starter-aop:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_OAUTH2_CLIENT = "org.springframework.boot:spring-boot-starter-oauth2-client:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_SECURITY = "org.springframework.boot:spring-boot-starter-security:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_TEST = "org.springframework.boot:spring-boot-starter-test:${Versions.SPRING_BOOT}"
        const val SPRING_BOOT_STARTER_WEB = "org.springframework.boot:spring-boot-starter-web:${Versions.SPRING_BOOT}"
        const val SPRING_FRAMEWORK_CONTEXT = "org.springframework:spring-context:${Versions.SPRING_FRAMEWORK}"
        const val SPRING_FRAMEWORK_TEST = "org.springframework:spring-test:${Versions.SPRING_FRAMEWORK}"
        const val SPRING_FRAMEWORK_WEB = "org.springframework:spring-web:${Versions.SPRING_FRAMEWORK}"
        const val SPRING_SECURITY_CONFIG = "org.springframework.security:spring-security-config:${Versions.SPRING_SECURITY}"
        const val SPRING_SECURITY_CORE = "org.springframework.security:spring-security-core:${Versions.SPRING_SECURITY}"
        const val SPRING_SECURITY_TEST = "org.springframework.security:spring-security-test:${Versions.SPRING_SECURITY}"
        // JJWT
        const val JJWT_API = "io.jsonwebtoken:jjwt-api:${Versions.JJWT}"
        const val JJWT_IMPL = "io.jsonwebtoken:jjwt-impl:${Versions.JJWT}"
        const val JJWT_JACSON = "io.jsonwebtoken:jjwt-jackson:${Versions.JJWT}"
        // Others
        const val APACHE_HTTPCLIENT = "org.apache.httpcomponents.client5:httpclient5:${Versions.APACHE_HTTPCLIENT}"
        const val ARCHUNIT = "com.tngtech.archunit:archunit:${Versions.ARCHUNIT}"
        const val ASPECTJ_ASPECTJRT = "org.aspectj:aspectjrt:${Versions.ASPECTJRT}"
        const val ASPECTJ_ASPECTJWEAVER = "org.aspectj:aspectjweaver:${Versions.ASPECTJRT}"
        const val FREEMARKER = "org.freemarker:freemarker:${Versions.FREEMARKER}"
        const val GSON = "com.google.code.gson:gson:${Versions.GSON}"
        const val JACKSON_ANNOTATION = "com.fasterxml.jackson.core:jackson-annotations:${Versions.JACKSON}"
        const val JACKSON_DATAFORMAT_YAML = "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:${Versions.JACKSON}"
        const val JACKSON_MODULE_KOTLIN = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.JACKSON}"
        const val LOG4J_API = "org.apache.logging.log4j:log4j-api:${Versions.LOG4J}"
        const val MAPSTRUCT = "org.mapstruct:mapstruct:${Versions.MAPSTRUCT}"
        const val MAPSTRUCT_PROCESSOR = "org.mapstruct:mapstruct-processor:${Versions.MAPSTRUCT}"
        const val OPENAPI = "org.springdoc:springdoc-openapi-webmvc-core:${Versions.OPENAPI}"
        const val QUERYDSL_JPA = "com.querydsl:querydsl-jpa:${Versions.QUERYDSL}"
        const val SNAKEYAML = "org.yaml:snakeyaml:${Versions.SNAKEYAML}"
        const val SWAGGER_ANNOTATIONS = "io.swagger.core.v3:swagger-annotations:${Versions.SWAGGER}"
        const val SWAGGER_MODELS = "io.swagger.core.v3:swagger-models:${Versions.SWAGGER}"
        const val TESTCONTAINERS = "org.testcontainers:testcontainers:${Versions.TESTCONTAINERS}"
        const val TESTCONTAINERS_RABBITMQ = "org.testcontainers:rabbitmq:${Versions.TESTCONTAINERS}"
        const val TESTCONTAINERS_SPOCK = "org.testcontainers:spock:${Versions.TESTCONTAINERS}"
    }
}

fun Project.allProjectDeps(){
    dependencies {
        "implementation" (Conf.Deps.KOTLIN_REFLECT)
        "implementation" (Conf.Deps.KOTLIN_STDLIN_JDK8)
        "implementation" (kotlin(Conf.Deps.STDLIB))
        "testImplementation" (Conf.Deps.SPOCK_CORE)
        "testImplementation" (Conf.Deps.GROOVY)
    }
}