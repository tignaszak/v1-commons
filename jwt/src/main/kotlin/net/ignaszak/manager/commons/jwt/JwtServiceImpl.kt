package net.ignaszak.manager.commons.jwt

import io.jsonwebtoken.*
import io.jsonwebtoken.SignatureAlgorithm.HS512
import io.jsonwebtoken.security.Keys
import net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_AUDIENCE
import net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_ISSUER
import net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_PARAM_ROLES
import net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_PARAM_TYPE
import net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_PREFIX
import net.ignaszak.manager.commons.jwt.JwtConstants.TOKEN_TYPE
import net.ignaszak.manager.commons.jwt.exception.JwtException
import net.ignaszak.manager.commons.jwt.mapper.JwtMapper
import net.ignaszak.manager.commons.jwt.mapper.JwtMapperImpl
import net.ignaszak.manager.commons.jwt.model.JwtUserModel
import net.ignaszak.manager.commons.jwt.to.JwtTO
import net.ignaszak.manager.commons.jwt.to.JwtUserTO
import net.ignaszak.manager.commons.utils.JsonUtils.toJson
import net.ignaszak.manager.commons.utils.JsonUtils.toObject
import java.lang.Exception
import java.lang.System.currentTimeMillis
import java.nio.charset.Charset
import java.util.*
import java.util.stream.Collectors

class JwtServiceImpl(private val jwtExpirationMs: Long, private val jwtSecret: String) : JwtService {

    private companion object {
        val jwtMapper: JwtMapper = JwtMapperImpl()
    }

    override fun buildToken(jwtUserTO: JwtUserTO, roles: Set<String>): String {
        val jwtUserModel = jwtMapper.jwtUserTOToJwtUserModel(jwtUserTO)

        return TOKEN_PREFIX + Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(toByteArray(jwtSecret)), HS512)
            .setHeaderParam(TOKEN_PARAM_TYPE, TOKEN_TYPE)
            .setIssuer(TOKEN_ISSUER)
            .setAudience(TOKEN_AUDIENCE)
            .setSubject(toJson(jwtUserModel))
            .setExpiration(Date(currentTimeMillis() + jwtExpirationMs))
            .claim(TOKEN_PARAM_ROLES, roles)
            .compact()
    }

    override fun getJwtTO(token: String): JwtTO {
        if (!validTokenString(token)) throw JwtException("Invalid authentication token")

        try {
            val parsedToken = Jwts.parserBuilder()
                .setSigningKey(toByteArray(jwtSecret))
                .build()
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))

            val jwtUserModelJson = parsedToken.body.subject

            val jwtUserModel = toObject(jwtUserModelJson, JwtUserModel::class.java)

            return JwtTO(
                jwtMapper.jwtUserModelToJwtUserTO(jwtUserModel),
                toRoleSet(parsedToken)
            )

        } catch (e: ExpiredJwtException) {
            throw JwtException("Request to parse expired JWT", e)
        } catch (e: UnsupportedJwtException) {
            throw JwtException("Request to parse unsupported JWT", e)
        } catch (e: MalformedJwtException) {
            throw JwtException("Request to parse invalid JWT", e)
        } catch (e: IllegalArgumentException) {
            throw JwtException("Request to parse empty or null JWT", e)
        } catch (e: Exception) {
            throw JwtException("JWT error occured", e)
        }
    }

    override fun validTokenString(token: String): Boolean {
        return token.isNotBlank() && token.startsWith(TOKEN_PREFIX)
    }

    override fun getJwtTimeToLiveMs(): Long {
        return jwtExpirationMs
    }

    private fun toByteArray(value: String): ByteArray {
        return value.toByteArray(Charset.forName("utf-8"))
    }

    private fun toRoleSet(parsedToken: Jws<Claims>): Set<String> {
        val roles = parsedToken.body[TOKEN_PARAM_ROLES] as List<*>
        return roles.stream()
            .map { role: Any? -> role!!.toString() }
            .collect(Collectors.toSet())
    }
}