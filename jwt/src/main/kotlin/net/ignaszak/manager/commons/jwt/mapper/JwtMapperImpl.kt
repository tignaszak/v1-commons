package net.ignaszak.manager.commons.jwt.mapper

import net.ignaszak.manager.commons.jwt.model.JwtUserModel
import net.ignaszak.manager.commons.jwt.to.JwtUserTO

class JwtMapperImpl : JwtMapper {
    override fun jwtUserModelToJwtUserTO(jwtUserModel: JwtUserModel): JwtUserTO {
        return JwtUserTO(jwtUserModel.publicId, jwtUserModel.email)
    }

    override fun jwtUserTOToJwtUserModel(jwtUserTO: JwtUserTO): JwtUserModel {
        return JwtUserModel(jwtUserTO.publicId, jwtUserTO.email)
    }
}