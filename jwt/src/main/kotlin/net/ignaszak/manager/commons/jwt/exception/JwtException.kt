package net.ignaszak.manager.commons.jwt.exception

import java.lang.RuntimeException

class JwtException : RuntimeException {
    constructor(message: String): super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}