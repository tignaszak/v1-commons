package net.ignaszak.manager.commons.jwt.model

data class JwtUserModel(val publicId: String, val email: String)
