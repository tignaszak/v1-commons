package net.ignaszak.manager.commons.jwt.to

data class JwtUserTO(val publicId: String, val email: String)
