package net.ignaszak.manager.commons.jwt.to

data class JwtTO(val jwtUserTO: JwtUserTO, val roles: Set<String>)
