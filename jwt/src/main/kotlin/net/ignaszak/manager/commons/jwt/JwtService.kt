package net.ignaszak.manager.commons.jwt

import net.ignaszak.manager.commons.jwt.to.JwtTO
import net.ignaszak.manager.commons.jwt.to.JwtUserTO

interface JwtService {

    fun buildToken(jwtUserTO: JwtUserTO, roles: Set<String>): String
    fun getJwtTO(token: String): JwtTO
    fun validTokenString(token: String): Boolean
    fun getJwtTimeToLiveMs(): Long
}