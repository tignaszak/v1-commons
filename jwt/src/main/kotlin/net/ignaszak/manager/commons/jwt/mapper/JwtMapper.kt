package net.ignaszak.manager.commons.jwt.mapper

import net.ignaszak.manager.commons.jwt.model.JwtUserModel
import net.ignaszak.manager.commons.jwt.to.JwtUserTO

interface JwtMapper {
    fun jwtUserTOToJwtUserModel(jwtUserTO: JwtUserTO): JwtUserModel
    fun jwtUserModelToJwtUserTO(jwtUserModel: JwtUserModel): JwtUserTO
}