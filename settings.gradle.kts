rootProject.name = "manager-commons"

include(
    "conf",
    "email",
    "error",
    "http-client",
    "jwt",
    "messaging",
    "openapi",
    "querydsl",
    "rest",
    "rest-criteria",
    "spring",
    "test",
    "user",
    "utils"
)
