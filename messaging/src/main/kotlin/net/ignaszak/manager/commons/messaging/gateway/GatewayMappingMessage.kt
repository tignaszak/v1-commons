package net.ignaszak.manager.commons.messaging.gateway

import java.io.Serializable

data class GatewayMappingMessage(val mappings: Set<Mapping>) : Serializable {
    data class Mapping(val path: String, val type: String) : Serializable
}
