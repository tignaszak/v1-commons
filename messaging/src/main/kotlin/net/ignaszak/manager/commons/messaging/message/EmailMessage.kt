package net.ignaszak.manager.commons.messaging.message

import java.io.Serializable

data class EmailMessage(
    val to: String,
    val from: String,
    val subject: String,
    val body: String
) : Serializable