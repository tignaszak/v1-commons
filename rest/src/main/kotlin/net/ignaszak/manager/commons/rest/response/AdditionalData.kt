package net.ignaszak.manager.commons.rest.response

interface AdditionalData {
    val additionalData: Any?
}