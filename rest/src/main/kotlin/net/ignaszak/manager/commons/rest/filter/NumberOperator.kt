package net.ignaszak.manager.commons.rest.filter

class NumberOperator(first: Operator<Set<Int>>?, second: Operator<Set<Int>>?) : BetweenOperator<Set<Int>>(first, second)