package net.ignaszak.manager.commons.rest.filter

open class BetweenOperator<T>(val first: Operator<T>?, val second: Operator<T>?)