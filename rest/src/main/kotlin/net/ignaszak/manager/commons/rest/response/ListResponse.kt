package net.ignaszak.manager.commons.rest.response

import io.swagger.v3.oas.annotations.media.Schema
import net.ignaszak.manager.commons.rest.page.PageResponse

@Schema(title = "Data collection response")
data class ListResponse<T>(
    @field:Schema(description = "Page response") val page: PageResponse,
    @field:Schema(description = "Response data collection") val list: Collection<T>
) : Response(Type.LIST)
