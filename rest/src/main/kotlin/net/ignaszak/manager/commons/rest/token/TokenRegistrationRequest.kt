package net.ignaszak.manager.commons.rest.token

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Token registration request")
data class TokenRegistrationRequest(
    @field:Schema(description = "User public id") val userPublicId: String,
    @field:Schema(description = "User jwt") val jwt: String
)
