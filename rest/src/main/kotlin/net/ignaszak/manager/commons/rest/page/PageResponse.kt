package net.ignaszak.manager.commons.rest.page

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Page response")
data class PageResponse(
    @field:Schema(description = "Page number") val number: Int,
    @field:Schema(description = "Element count per page") val size: Int,
    @field:Schema(description = "Total pages count") val totalPages: Int,
    @field:Schema(description = "Total elements count") val totalElements: Long
)
