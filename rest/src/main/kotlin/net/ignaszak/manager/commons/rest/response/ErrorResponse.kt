package net.ignaszak.manager.commons.rest.response

import io.swagger.v3.oas.annotations.media.Schema
import java.util.Collections.singleton

@Schema(title = "Error response")
data class ErrorResponse(
    @field:Schema(description = "Error information list") val errors: Set<ErrorInfoResponse>
) : Response(Type.ERROR) {
    companion object {
        @JvmStatic
        fun of(code: String, message: String): ErrorResponse {
            return ErrorResponse(singleton(ErrorInfoResponse(code, message)))
        }
    }
}
