package net.ignaszak.manager.commons.rest.response

import com.fasterxml.jackson.annotation.JsonIgnore
import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Single data response")
data class DataResponse<T>(
    @field:Schema(description = "Response data") val data: T,
    @JsonIgnore override val additionalData: Any?
) : Response(Type.DATA), AdditionalData {
    constructor(data: T) : this(data, null)
}
