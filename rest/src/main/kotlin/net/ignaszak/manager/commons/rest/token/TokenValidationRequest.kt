package net.ignaszak.manager.commons.rest.token

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Token validation request")
data class TokenValidationRequest(@field:Schema(description = "User jwt") val jwt: String)
