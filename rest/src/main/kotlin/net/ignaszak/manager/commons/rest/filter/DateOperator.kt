package net.ignaszak.manager.commons.rest.filter

import java.time.LocalDateTime

class DateOperator(first: Operator<Set<LocalDateTime>>?, second: Operator<Set<LocalDateTime>>?) : BetweenOperator<Set<LocalDateTime>>(first, second)