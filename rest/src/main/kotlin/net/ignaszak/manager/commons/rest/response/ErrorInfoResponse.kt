package net.ignaszak.manager.commons.rest.response

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Error information", description = "")
data class ErrorInfoResponse(
    @field:Schema(description = "Error code") val code: String,
    @field:Schema(description = "Error message") val message: String
)
