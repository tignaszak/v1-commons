package net.ignaszak.manager.commons.rest.token

import io.swagger.v3.oas.annotations.media.Schema

@Schema(title = "Token activation request")
data class TokenActivationRequest(@field:Schema(description = "User public id") val userPublicId: String)
