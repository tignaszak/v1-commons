package net.ignaszak.manager.commons.rest.response

import io.swagger.v3.oas.annotations.media.Schema

open class Response(@field:Schema(description = "Response type") val type: Type) {
    enum class Type {
        DATA, LIST, ERROR
    }
}