package net.ignaszak.manager.commons.rest.page

import io.swagger.v3.oas.annotations.Parameter

data class PageRequest(@Parameter(required = false, description = "Page number") val page: Int?)
