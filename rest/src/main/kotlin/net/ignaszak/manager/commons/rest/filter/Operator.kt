package net.ignaszak.manager.commons.rest.filter

import com.google.gson.annotations.SerializedName

open class Operator<V>(val type: Type, val value: V) {
    enum class Type(val character: String) {
        @SerializedName(":")  EQUALS(":"),
        @SerializedName("!:") NOT_EQUALS("!:"),
        @SerializedName("::")  CONTAINS("::"),
        @SerializedName("!::") NOT_CONTAINS("!::"),
        @SerializedName(">")  GREATER(">"),
        @SerializedName(">:") GREATER_OR_EQUALS(">:"),
        @SerializedName("<")  LOWER("<"),
        @SerializedName("<:") LOWER_OR_EQUALS("<:")
    }
}
